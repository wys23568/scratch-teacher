
/**
 * 我的作品
 * */
import React from 'react'
import './index.less'

import {
    Route,
    Link,
} from "react-router-dom";
import {deleteWorksFetch, myworksFetch} from "../../service/courseFetch";
import {message,Spin,Pagination,Modal} from "antd";
import GeneralEmpty from "../../components/generalEmpty";
import moment from "moment/moment";
import SimplePop from "../../components/simplePop";
import {jumpApi, LOGINTOKEN} from "../../utils/constants";
import {GetHrefQueryString, GetSearchQueryString, updateQueryStringParameter} from "../../utils/commenutil";

 class MyWorks  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            selectedIndex:0,
            data:[],
            totalSize:0,
            currentPage:0,
            delectVisible:false,
            currentItem:null,
            delectContent:'',
        };
        console.log('MyWorks constructor ');

    }
    // componentWillMount() {
    //     console.log('MyWorks componentWillMount ');
    // }
    componentDidMount(){
        console.log('MyWorks componentDidMount ');
        let params = new URLSearchParams(this.props.location.search);
        let type = params.get('type');
        let page = params.get('page');
        console.log('MyWorks props URLSearchParams type:', type,'page',page);
        this.state.selectedIndex = type?parseInt(type):0;
        this.state.totalSize = 0;
        this.state.currentPage = page?parseInt(page):0;
        this.listFetch.bind(this)();
    }
     static getDerivedStateFromProps(nextProps, prevState) {
         console.log('MyWorks getDerivedStateFromProps \n nextProps', nextProps,'\n prevState', prevState);
         let params = new URLSearchParams(nextProps.location.search);
         let type = params.get('type');
         let page = params.get('page');
         console.log('MyWorks getDerivedStateFromProps  nextProps URLSearchParams type:', type,'page',page);
         if (type&&parseInt(type)!==prevState.selectedIndex) {
             console.log('MyWorks getDerivedStateFromProps 2 URLSearchParams type:', type,'page',page);
             return {
                 ...prevState,
                 selectedIndex: parseInt(type)
             }
         }
         return null
     }
    // componentWillReceiveProps(nextProps){
    //     console.log('MyWorks componentWillReceiveProps',nextProps);
    //     let loginToken =  localStorage.getItem(LOGINTOKEN);
    //     if (!loginToken ||loginToken.length===0){
    //         console.log('MyWorks componentWillReceiveProps loginToken',loginToken);
    //         return;
    //     }
    //     let params = new URLSearchParams(nextProps.location.search);
    //     let type = params.get('type');
    //     let page = params.get('page');
    //     console.log('MyWorks props URLSearchParams type:', type,'page',page);
    //     // this.state.selectedIndex = type?parseInt(type):0;
    //     // this.state.totalSize = 0;
    //     // this.state.currentPage = page?parseInt(page):0;
    //     // this.listFetch.bind(this)();
    // }
    componentDidUpdate(prevProps, prevState, snapshot){
        let params = new URLSearchParams(prevProps.location.search);
        let type = params.get('type');
        let page = params.get('page');
        console.log('MyWorks componentDidUpdate prevProps URLSearchParams type:', type,'page',page);
        console.log('MyWorks componentDidUpdate \n prevProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot,'\n this.state.selectedIndex',this.state.selectedIndex );
        if (this.state.selectedIndex !== prevState.selectedIndex)
        {
            console.log('MyWorks componentDidUpdate listFetch');
            let loginToken =  localStorage.getItem(LOGINTOKEN);
            if (!loginToken ||loginToken.length===0){
                console.log('MyWorks componentDidUpdate loginToken =',loginToken);
                return;
            }
            this.state.currentPage = 0;
            this.listFetch.bind(this)();
        }
    }
    render(){

        const {hasPublicCount = 0,noPublicCount = 0}= this.state;
        let barArray = [
            {
                title: '已发布的作品',
                count:hasPublicCount,
            },
            {
                title: '未发布的作品',
                count:noPublicCount,
            }
        ]
        console.log('MyWorks render ');

        return <div className='my-works-root'>
            <div className='header-bar'>
                {
                    barArray.map((item,index)=>{

                        let style ={marginLeft:'35px'};
                        if (index===0)
                        {
                            style ={marginLeft:'32px'};
                        }
                        let titleClass = this.state.selectedIndex === index?'title select':'title';
                        return <div className='item' key={index} style={style} onClick={()=>{
                            this.props.history.push({
                                pathname: '/personal-center/works',
                                search: `?type=${index}`
                            });
                            // this.setState({
                            //     selectedIndex:index,
                            //     totalSize:0,
                            //     currentPage:0,
                            // },()=>{
                            //     // this.listFetch.bind(this)();
                            // })
                        }}>
                            <div className={titleClass}>
                                {`${item.title}\xa0\xa0(${item.count})`}
                            </div>
                            {this.state.selectedIndex === index? <div className='bottom-line'></div>:''}
                        </div>
                    })
                }
                </div>
            {this.showBody.bind(this)()}
            <Modal
                visible={this.state.delectVisible}
                footer={null}
                closable={false}
                width='386px'
                style={{
                    marginTop:'140px'
                }}
                bodyStyle={{
                    padding:'0px',
                }}
                onOk={this.simplePopOnOk.bind(this)}
                onCancel={this.simplePopOnCancel.bind(this)}
            >
                <SimplePop content={this.state.delectContent}
                           okTitle='删除'
                           onCancelPop={this.simplePopOnCancel.bind(this)}
                           onOkPop={this.simplePopOnOk.bind(this)}
                />
            </Modal>
        </div>
    }
    showBody = ()=>{
        if (this.state.isLoading)
        {
            return <Spin size="large" style={{"fontSize":"30px","display":'block','margin':'320px auto'}}/>;
        }
        if (!this.state.data || this.state.data.length === 0)
        {
            return <GeneralEmpty hintText={'暂无作品'}/>
        }
        if (this.state.selectedIndex === 0)
        {
            return <div className='public-list-root'>
                  <div className='top'>
                    {
                        this.state.data.map((item,index)=>{

                            const {coverUrl,name,lookCount=0,likeCount=0,updatedAt} = item;
                            let updatedStr = moment(updatedAt).format("YYYY-MM-DD");

                            return <div key = {index} className='public-list-row'>
                                <img className='coverUrl' src={coverUrl} alt='' onClick={this.goWorksDetail.bind(this,item)}/>
                                <div className='name'>{name?name:'\xa0'}</div>
                                <div className='update-time'>更新时间：{updatedStr}</div>
                                <div className='look-like'>
                                <span className='look-bg'>
                                    <img className='look-img' src={require('../../static/img/ic_look.png')} alt=''/>
                                    <span className='look-count'>{lookCount}</span>
                                </span>
                                    <span className='like-bg'>
                                    <img className='like-img' src={require('../../static/img/ic_like.png')} alt=''/>
                                    <span className='like-count'>{likeCount}</span>
                                </span>
                                </div>
                                <div className='bottom'>
                                    <div className='edit' onClick={this.editWorksClick.bind(this,item)}>
                                        <img className='edit-img' src={require('../../static/img/ic_edit.png')} alt=''/>
                                        <span className='edit-title'>编辑</span>
                                    </div>
                                    <div className='divider'></div>
                                    <div className='edit' onClick={this.delectWorks.bind(this,item)}>
                                        <img className='edit-img' src={require('../../static/img/ic_delect_works.png')} alt=''/>
                                        <span className='edit-title'>删除</span>
                                    </div>
                                </div>
                            </div>
                        })
                    }
                </div>
                <div className='pagination'>
                    <Pagination pageSize={8} total={this.state.totalSize}
                                current={this.state.currentPage+1}
                                hideOnSinglePage={true}
                                onChange={this.onChangePage.bind(this)}/>
                </div>
            </div>
        }
        else if(this.state.selectedIndex === 1)
        {
            return <div className='no-public-list-root'>
                <div className='top'>
                    {
                        this.state.data.map((item,index)=>{

                            const {coverUrl,name,updatedAt} = item;
                            let updatedStr = moment(updatedAt).format("YYYY-MM-DD");

                            return <div key = {index} className='public-list-row'>
                                <img className='coverUrl' src={coverUrl} alt='' onClick={this.sendWorksClick.bind(this,item)}/>
                                <div className='name'>{name?name:'\xa0'}</div>
                                <div className='update-time'>更新时间：{updatedStr}</div>
                                <div className='bottom'>
                                    <div className='edit' onClick={this.sendWorksClick.bind(this,item)}>
                                        <img className='edit-img' src={require('../../static/img/ic_send.png')} alt=''/>
                                        <span className='edit-title'>发布</span>
                                    </div>
                                    <div className='divider'></div>
                                    <div className='edit' onClick={this.editWorksClick.bind(this,item)}>
                                        <img className='edit-img' src={require('../../static/img/ic_edit.png')} alt=''/>
                                        <span className='edit-title'>编辑</span>
                                    </div>
                                    <div className='divider'></div>
                                    <div className='edit' onClick={this.delectWorks.bind(this,item)}>
                                        <img className='edit-img' src={require('../../static/img/ic_delect_works.png')} alt=''/>
                                        <span className='edit-title'>删除</span>
                                    </div>
                                </div>
                            </div>
                        })
                    }
                </div>
                <div className='pagination'>
                    <Pagination pageSize={8} total={this.state.totalSize}
                                current={this.state.currentPage+1}
                                hideOnSinglePage={true}
                                onChange={this.onChangePage.bind(this)}/>
                </div>
            </div>
        }

    }
    sendWorksClick = (item)=>{
        console.log('点击发布',this.props.history);
        const {id, content, name, coverUrl} = item;
        this.props.history.push({
            pathname: '/publish-works',
            search: `?productionId=${id}&content=${content}&name=${name}&coverUrl=${coverUrl}`
        })

    }
    editWorksClick = ({id})=>{
        console.log('点击编辑')
        window.location.href = `${jumpApi}?type=3&productionId=${id}`
    }
    delectWorks = (item)=>{
        console.log('点击删除')
        this.setState({
            currentItem:item,
            delectContent:`确定要删除作品“${item.name}”吗？`,
            delectVisible:true,
        })
    }
    simplePopOnCancel = ()=>{
        this.setState({
            delectVisible:false,
        })
    }
    simplePopOnOk = ()=>{
        this.setState({
            delectVisible:false,
        })
        const {currentItem,data}= this.state;
        let loginToken =  localStorage.getItem(LOGINTOKEN);
        deleteWorksFetch(loginToken,currentItem.id).then((result)=>{
            if (result.data && result.data.code === 200)
            {
                if (data&&data.length ===1)
                {
                    if (this.state.currentPage > 0)
                    {
                        this.state.currentPage = this.state.currentPage -1;
                        console.log('document url ',updateQueryStringParameter(document.URL,'page', this.state.currentPage));
                        let newUrl = updateQueryStringParameter(document.URL,'page',this.state.currentPage+'');
                        window.history.replaceState(null,null,newUrl);
                    }
                    this.listFetch();
                }
                else {
                    this.listFetch();
                }

            }
            else
            {
                message.warning(result.data.error);
            }
        }).catch((error)=>{

        });
    }
    listFetch = ()=>{
        let loginToken =  localStorage.getItem(LOGINTOKEN);

        this.setState({
            isLoading:true,
        })
        let state;
        if (this.state.selectedIndex ===0)
        {
            state = 2;
        }
        else if(this.state.selectedIndex ===1)
        {
            state = 1;
        }
        myworksFetch(loginToken,state,this.state.currentPage).then((result)=>{

            if (result.data && result.data.code === 200)
            {

                let data = result.data.data.content;
                let extendParam = result.data.data.extendParam;
                let hasPublicCount = 0;
                let noPublicCount = 0;
                for (let value of extendParam)
                {
                    if (value.t === 'A'){
                        noPublicCount = value.c;
                    }
                    if (value.t === 'B'){
                        hasPublicCount = value.c;
                    }
                }
                // if (state ===2)
                // {
                //     let aTemp = data[0];
                //     for (var i=0;i<6;i++)
                //     {
                //         var newObj = JSON.parse(JSON.stringify(aTemp));
                //         data.push(newObj);
                //     }
                // }

                this.setState({
                    data:data,
                    totalSize:result.data.data.pageable.totalSize,
                    hasPublicCount:hasPublicCount,
                    noPublicCount:noPublicCount,
                    isLoading:false,
                })
            }
            else
            {
                message.warning(result.data.error);
                this.setState({
                    isLoading:false,
                })
            }

        }).catch((error)=>{


        });
    }
    onChangePage = (page, pageSize)=>{
        console.log('onChangePage',page,pageSize);
        this.state.currentPage = page-1;
        console.log('document url ',updateQueryStringParameter(document.URL,'page', this.state.currentPage));
        let newUrl = updateQueryStringParameter(document.URL,'page',this.state.currentPage+'');
        window.history.replaceState(null,null,newUrl);
        this.listFetch();
    }
    goWorksDetail = ({id})=>{
        console.log('document.URL',document.URL,document.URL.split('#'));
        let  url = document.URL.split('#')[0];
        if (url)
        {
            window.open(`${url}#/works-detail/${id}`);
        }
        // this.props.history.push(`/works-detail/${id}`)
    }

}
export default MyWorks;
