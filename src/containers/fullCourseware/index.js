
/**
 * 完整课件
 * */
import React from 'react'
import './index.less'
import Header from "../../components/header";
import Content from "../../components/content";
import {pdfUrl} from "../../utils/constants";

export  default  class FullCourFseware  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {

        };

      let courseware = localStorage.getItem('courseware');

        console.log('FullCourFseware constructor ',courseware);

    }
    componentWillMount() {
        console.log('FullCourFseware componentWillMount ');
    }
    componentDidMount(){
        console.log('FullCourFseware componentDidMount ');
    }
    componentWillReceiveProps(nextProps){
        console.log('FullCourFseware componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('FullCourFseware \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){
        let titles = localStorage.getItem('courseTitle');
        titles = JSON.parse(titles);
        let title1 = titles.title1;
        let title2 = titles.title2;
        let title3 = titles.title3;
        let title4 = titles.title4;
        let title5 = null;
        let title6 = null;

        let type = titles.type;
        if (type === 2)
        {
            title4 = '课件';
        }
        else if(type === 3)
        {
            title5 = '课件';
        }
        //https://test.huazilive.com/HuaziHudong/HudongWeb/百度新闻.pdf
        //https://test.huazilive.com/HuaziHudong/HudongWeb/mac.doc
        //https://test.huazilive.com/HuaziHudong/HudongWeb/mac终端操作.docx
        //https://test.huazilive.com/HuaziHudong/HudongWeb/mp4.mp4
        let courseware = localStorage.getItem('courseware');
        return <div>
            <Header type={type} hiddenCreation={true} title1={title1} title2 = {title2} title3={title3} title4={title4} title5={title5} title6={title6}/>
            <Content>
                <iframe src={`${pdfUrl}${courseware}`}
                        className='full-courseware'
                />
                {/*<iframe src={'http://localhost:8888/web/viewer.html?file=https://huazi-api-test.oss-cn-beijing.aliyuncs.com/storage/4038/252812/XMjekZRxBCsqZeuOrQRiB6emlAgi60y.pdf'}*/}
                        {/*className='full-courseware'*/}
                {/*/>*/}
                {/*<iframe src={'https://mozilla.github.io/pdf.js/web/viewer.html'}*/}
                        {/*className='full-courseware'*/}
                {/*/>*/}
            </Content>
        </div>
    }

}
