const { override, fixBabelImports, addLessLoader, useBabelRc } = require('customize-cra');
process.env.GENERATE_SOURCEMAP = "false";// 去掉map
module.exports = override(
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true,
    }),
    addLessLoader({
        javascriptEnabled: true,
        modifyVars: { '@primary-color': '#54C3D4' },
    }),
    useBabelRc()
);
