
/**
 * 老师评语弹框
 * */
import React from 'react'
import './index.less'

import { Button,Input} from 'antd';
const { TextArea } = Input;

export  default  class CommentsPop  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            textValue:props.textValue?props.textValue:'',
        };
        console.log('CommentsPop constructor');
    }
    componentWillMount() {
        console.log('CommentsPop componentWillMount ');
    }
    componentDidMount(){
        console.log('CommentsPop componentDidMount ');
    }
    componentWillReceiveProps(nextProps){
        console.log('CommentsPop componentWillReceiveProps',nextProps);
        if (nextProps.textValue!==this.state.textValue)
        {
            this.setState({
                textValue:nextProps.textValue,
            })
        }
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('CommentsPop componentDidUpdate \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){
        return <div className='comments-root'>
            <div className='title'>老师评语</div>
            <div className='name'>{this.props.studentName}</div>
            <img className='close' src={require('../../static/img/icon_close.jpg')} onClick={this.onCancelPop.bind(this)}/>
            <TextArea className='textArea' value={this.state.textValue} placeholder="请输入对学生作品的评价～"  maxLength={300} onChange={this.onChange.bind(this)}/>
            <div className='text-lenght-tip'>{this.state.textValue.length}/300</div>
            <Button type="primary" className="complete" onClick={this.onOkPop.bind(this)}>完成</Button>
        </div>
    }
    onCancelPop =()=>{
        this.props.onCancelPop();
    }
    onChange =(e)=>{
        console.log('text ',e.target.value)
        this.setState({
            textValue:e.target.value,
        })
    }
    onOkPop =()=>{
        this.props.onOkPop(this.state.textValue);
    }

}
