

import React from 'react'
import {
    Route,
    Link,
} from "react-router-dom";
import './index.less'

import CourseManagement from "../courseManagement";
import { Layout, Row, Col, Icon } from 'antd';
import PersonalCenter from "../personalCenter";
import {saveLoginInfo} from "../../redux/actions/loginactions";
import {connect} from "react-redux";
import ClassRoom from "../classroom";
import FullCourFseware from "../fullCourseware";
import studentWork from "../studentWork";
import StudentLeaveMessage from "../studentLeaveMessage";
import CheckMoreCourse from "../checkMoreCourse";
import CourseLessonList from "../courseLessonList";
const { Header, Content } = Layout;

@connect()
class Layouts  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {

        };
        console.log('Layouts constructor ',this);
    }
    componentDidMount(){
        console.log('Layouts componentDidMount');
    }
    render(){
        return<div className="root-bg">
            {/*<Header className='header-home'>*/}
                {/*<div className="logo" />*/}
                {/*<Row type='flex' justify='space-between'>*/}
                    {/*<Row type='flex' style={{ minWidth: '400px' }} >*/}
                        {/*<Col span={4}><Link className='link-user' to='/'>课程管理</Link></Col>*/}
                    {/*</Row>*/}
                    {/*<Row type='flex' justify='end' style={{ minWidth: '500px' }}>*/}
                        {/*<Col span={5}><div><Link className='creation-user' to='/creation'>我要创作</Link></div></Col>*/}
                        {/*<Col span={5}><div><Link className='head-portrait' to='/personal-center'>头像</Link></div></Col>*/}
                        {/*<Col span={5}><div onClick={this.logout.bind(this)}>退出登录</div></Col>*/}
                    {/*</Row>*/}
                {/*</Row>*/}
            {/*</Header>*/}
            {/*<Route exact path="/"  render={(props)=>{return<CourseManagement {...props} key={new Date()}/>}  }/>*/}
            <Route exact path="/"  component={CourseManagement} />
            <Route path="/course-lesson-list/:courseId/:teacherCourseId" component={CourseLessonList} />
            <Route path="/full-courseware/" component={FullCourFseware} />
            <Route path="/student-work/:courseId/:teacherCourseId/:courseLessonId" component={studentWork} />
            <Route path="/student-leavemessage/:courseId/:teacherCourseId/:courseLessonId" component={StudentLeaveMessage} />
            <Route path="/check-morecourse/" component={CheckMoreCourse} />
            <Route path="/class-room/:courseId/:teacherCourseId/:courseLessonId" component={ClassRoom} />
            <Route path="/personal-center" component={PersonalCenter} />
        </div>

    }


}

export  default   Layouts;
