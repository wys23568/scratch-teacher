
import {AxiosPost,AxiosApiServicePost} from "./axios";

export const teacherInfor=async(loginToken,useFlag,name,avatarUrl)=>{
    let data = {};
    data.loginToken = loginToken;
    if (useFlag === 2)
    {
        data.useFlag = useFlag;
        data.name = name;
        data.avatarUrl = avatarUrl;
    }
    return AxiosPost('/account/teacher/info',data)
};
//更新 用户信息
export const updateTeacherInfor=(loginToken,name,avatarUrl)=>{
    let data = {};
    data.loginToken = loginToken;
    if (name&&name.length)
    {
        data.name = name;
    }
    if (avatarUrl&&avatarUrl.length)
    {
        data.avatarUrl = avatarUrl;
    }
    return AxiosPost('/account/teacher/update',data)
};
//微信登录
export const weixinLogin = (accountType,code)=> {
    return AxiosApiServicePost('/account/common/sso/weixin/login', {
        accountType: accountType,
        code:code
    })
}

//第三方登录-绑定手机获取验证码
export function thirdPartyGetCode(ssoId,oauthId,verifyPhone) {
    const result = AxiosApiServicePost('/account/common/sso/binding/request', {
        ssoId: ssoId,
        oauthId: oauthId,
        verifyPhone: verifyPhone
    })
    return result
}
//第三方登录-绑定手机
export function thirdPartyBindphone(ssoId,oauthId,verifyPhone,verifyCode) {
    const result = AxiosApiServicePost('/account/common/sso/binding/confirm',{
        ssoId: ssoId,
        oauthId: oauthId,
        verifyPhone: verifyPhone,
        verifyCode: verifyCode,
    });
    return result
}


