
import {AxiosPost,AxiosApiServicePost,AxiosGet} from "./axios";

export const courseListFetch = (loginToken,type,issue,pageNumber=0,pageSize)=>{
    let data = {};
    data.loginToken = loginToken;
    if (type)
    {
        data.type = type;
    }
    if (issue)
    {
        data.issue = issue;
    }

    data.pageNumber = pageNumber;
    if (pageSize)
    {
        data.pageSize = pageSize;
    }
    return AxiosPost('/account/teacher/course/list',data)
};


export const courseLessonInfoListFetch = (loginToken,courseId,teacherCourseId)=>{

    return AxiosPost('/account/teacher/course/detail',{
        loginToken:loginToken,
        courseId:courseId,
        teacherCourseId:teacherCourseId,
    })
}


export  const  courseLessonDetailFetch = (loginToken,courseId,teacherCourseId,courseLessonId)=>{

    return AxiosPost('/account/teacher/course/lesson/detail',{
        loginToken:loginToken,
        courseId:courseId,
        teacherCourseId:teacherCourseId,
        courseLessonId:courseLessonId,
    })
}

export  const  homeworkCommentFetch = (loginToken,studentCourseHomeworkId,comment)=>{

    return AxiosPost('/account/teacher/course/student/homework/comment',{
        loginToken:loginToken,
        studentCourseHomeworkId:studentCourseHomeworkId,
        comment:comment,
    })
}

export  const  leaveMessageFetch = (loginToken,courseId,teacherCourseId,courseLessonId,sort=2,pageNumber=0,pageSize=10)=>{

    return AxiosPost('/account/teacher/course/lesson/homework/message/list',{
        loginToken:loginToken,
        courseId:courseId,
        teacherCourseId:teacherCourseId,
        courseLessonId:courseLessonId,
        sort:sort,
        pageNumber:pageNumber,
        pageSize:pageSize,
    })
}

export  const  homeworkMessageUpdateFetch = (loginToken,studentCourseHomeworkMessageIds,state)=>{

    return AxiosPost('/account/teacher/course/lesson/homework/message/update',{
        loginToken:loginToken,
        studentCourseHomeworkMessageIds:studentCourseHomeworkMessageIds,
        state:state,
    })
}

export  const  myworksFetch = (loginToken,state,pageNumber=0,pageSize=8)=>{

    return AxiosPost('/account/teacher/production/list',{
        loginToken:loginToken,
        state:state,
        pageNumber:pageNumber,
        pageSize:pageSize
    })
}
export  const  deleteWorksFetch = (loginToken,productionId)=>{

    return AxiosPost('/account/teacher/production/delete',{
        loginToken:loginToken,
        productionId:productionId,
    })
}

export const getProductionTagFetch =  () => {
    return AxiosGet('/config/productionTag')
}
export const getProductionCoverFetch =  () => {
    return AxiosGet('/config/productionCover')
}
export const postPublishInfoFetch =  (data) => {
    return AxiosPost('/account/teacher/production/publish', data)
}

export const worksDetailFetch = (loginToken,productionId) => {
    return AxiosPost('/account/teacher/production/detail', {
        loginToken:loginToken,
        productionId:productionId,
    })
}
// lookCount4AddFlag 1不增加、2增加
export const productionDetailFetch = (loginToken,productionId,lookCount4AddFlag=1) => {
    return AxiosPost('/production/detail', {
        loginToken:loginToken,
        productionId:productionId,
        lookCount4AddFlag:lookCount4AddFlag,
    })
}



