
/**
 * 获取code
 * */
import React from 'react'
import './index.less'
import AccountSettings from "../accountSettings";

import {
    Route,
    Link,
} from "react-router-dom";
import {GetSearchQueryString,GetHrefQueryString} from "../../utils/commenutil";
import {saveLoginInfo} from "../../redux/actions/loginactions";
import {teacherInfor, weixinLogin} from "../../service/personFetch";
import {message} from "antd/lib/index";
import {connect} from "react-redux";
import {LOGINTOKEN, USERINFO} from "../../utils/constants";

@connect()
class GetCode  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {

        };

        console.log('GetCode constructor ');

    }
    componentWillMount() {
        console.log('GetCode componentWillMount ');
    }
    componentDidMount(){
        console.log('GetCode componentDidMount ');
        this.getWeixinCode.bind(this)();
    }
    componentWillReceiveProps(nextProps){
        console.log('GetCode componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('GetCode \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    getWeixinCode = ()=>{
        // let loginLoading = localStorage.getItem("loginLoading");
        // if (parseInt(loginLoading))
        {
            let code =GetSearchQueryString('code');
            let code2 = GetHrefQueryString('code');

            let  appId='1106266783';
            let  appKey='2421951481';
            let  accountType='common';

            console.log(' search code:',code);
            console.log(' href code:',code2);
            console.log('this.URL',document.URL);
            let params = new URLSearchParams(window.location.search);
            console.log('URLSearchParams url:',params.toString());
            let params2 = new URLSearchParams(this.props.location.search);
            console.log('URLSearchParams2 url:',params2.toString());
            let code3 = params2.get('code');
            console.log('URLSearchParams2 code:',code3);

            if(code3!=null){
                console.log('this.URL',document.URL);
                // window.history.replaceState(null, null, 'http://127.0.0.1:3000');
                // console.log('window.history.replaceState');
                weixinLogin(appId,appKey,accountType,code).then((result)=>{
                    console.log('weixinLogin result',result.data);
                    if (result.data.code !== 200)
                    {
                        let data = result.data.data;
                        if (data.sso)
                        {
                            let ssoId = data.sso.id;
                            let oauthId = data.sso.oauthId;
                            let avatarUrl = data.sso.avatarUrl;
                            let nickname = data.sso.nickname;
                            //卓星账号
                            // let ssoId = 20111610;
                            // let oauthId = 'og_h10i1_l5muIu73VB-evPYY1Rk';
                            // let avatarUrl = 'http://thirdwx.qlogo.cn/mmopen/vi_32/uRSd4P8a94PhMJ5Lp4IydqrCnoS3iaMSYy53JSQQWIiaChEicTq0ibeLxHXqjsiaL6ZyDjiaYeHeFQTPDSwCejXAXFCA/132';
                            // let nickname = '飞  流 星';

                            //红涛账号
                            // let ssoId = 20111594;
                            // let oauthId = 'og_h10rR1Tc_7VMXMbxVj7lXGNdo';
                            // let avatarUrl = 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoLG6lTcYjVo7Zf1FVXf5ypj7fLK1BAAkwgTO5NzXnkmWGvSyicqVgg1k7ytCUCYUm6UzSlt9TeCQg/132';
                            // let nickname = '李红涛';
                            this.props.history.push(`/bind-phone?ssoId=${ssoId}&oauthId=${oauthId}&&avatarUrl=${avatarUrl}&name=${nickname}`);
                        }
                        else {

                            // let loginToken =  data.loginToken;
                            let loginToken = 'ae674f7ec1ce09b124ed21471e9780c4';
                            teacherInfor(loginToken).then((result)=>{
                                console.log('teacherInfor this',this,'\n result',result);
                                if (result.data && result.data.code === 200)
                                {
                                    localStorage.setItem(LOGINTOKEN, loginToken); //红涛
                                    localStorage.setItem(USERINFO,JSON.stringify(result.data.data));
                                    console.log('登录中');
                                    this.props.dispatch(saveLoginInfo(loginToken, result.data.data));
                                    console.log('登录中1');
                                    this.props.history.replace('/');
                                    console.log('登录中2');
                                }

                            }).catch((error)=>{

                            })

                        }

                    }
                    else
                    {
                        message.warning(result.data.error);
                    }

                }).catch((error)=>{


                });
                localStorage.setItem("loginLoading", '0');
            }

        }

    }

    render(){

        return <div>
            获取code
        </div>
    }

}
export default  GetCode;
