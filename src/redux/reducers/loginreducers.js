const initialState = {
    token:null,
    userinfo:null
}
export default (state =initialState, action) => {
    switch (action.type) {
        case 'saveLoginInfo':
        {
            const {token,userinfo}=action.loginInfo;
            return{
                ...state,token,userinfo
            }
        }
        case 'save':
        {
            const {token,userinfo}=action.loginInfo;
            return{
                ...state,token,userinfo
            }
        }
        default:
            return state
    }
}

