
/**
 * 课堂
 * */
import React from 'react'
import './index.less'
import Header from "../../components/header";
import Content from "../../components/content";
import { Button,Modal,message,Input,Icon,Spin} from 'antd';
import {courseLessonDetailFetch, homeworkCommentFetch} from "../../service/courseFetch";
import moment from "moment/moment";
import CommentsPop from "../../components/commentsPop";
import Myvideo from '../../components/Myvideo/Myvideo.js'
import {jumpApi, LOGINTOKEN} from "../../utils/constants";

const { TextArea } = Input;

export  default  class ClassRoom  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            data:{},
            allOnline:false,
            commentsVisible:false,
            visible:false,
            currentVideoUrl:null,
        };

        console.log('ClassRoom constructor ');

    }
    componentWillMount() {
        console.log('ClassRoom componentWillMount ');
    }
    componentDidMount(){
        console.log('ClassRoom componentDidMount ');
        this.lessonDetailFetch();
    }
    componentWillReceiveProps(nextProps){
        console.log('ClassRoom componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('ClassRoom \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){

        let titles = localStorage.getItem('courseTitle');
        titles = JSON.parse(titles);
        let title1 = titles.title1;
        let title2 = titles.title2;

        return <div>
            <Header type={2} title1={title1} title2 = {title2}  title3={'课程'}/>
            <Content>
                {this.showBody.bind(this)()}
            </Content>
            <Modal
                visible={this.state.commentsVisible}
                footer={null}
                closable={false}
                width='646px'
                bodyStyle={{
                    padding:'0px',
                }}
                onOk={()=>{
                    this.setState({
                        commentsVisible:false,
                    })
                }}
                onCancel={()=>{
                    this.setState({
                        commentsVisible:false,
                    })
                }}
            >
                <CommentsPop
                    studentName={this.state.currentItem&&this.state.currentItem.studentName}
                    textValue={this.state.currentItem&&this.state.currentItem.comment}
                    onCancelPop={()=>{
                    this.setState({
                        commentsVisible:false,
                    })
                    }}
                    onOkPop={(textValue)=>{
                        this.commentFetch.bind(this,textValue)();
                    }}
                />

            </Modal>
        </div>
    }
    showBody =()=>{
        if (this.state.isLoading){

            return <Spin size="large" style={{"fontSize":"30px","display":'block','margin':'320px auto'}}/>;
        }
        const {visible,currentVideoUrl} = this.state;
        return <div className='course-root'>
            <div className='course-class-root'>
                {this.courseClassHeader.bind(this)()}
                <div className='divider-line'></div>
                <div className='student-list-title'>
                    <img className='img-tip' src={require('../../static/img/person.png')} alt=''/>
                    <span className='title'>学员列表</span>
                    <span className="check-online" onClick={this.checkOnline.bind(this)}>
                            {this.state.allOnline?'显示所有学员':'只看线上学员'}
                        </span>
                </div>
                {this.studentList.bind(this)()}
                {visible && <div onClick={this.handleCancel} className='show-background'>
                    <div onClick={(e) => { e.stopPropagation() }} className='show-video-content'>
                        <Myvideo videoUrl={currentVideoUrl} handleCancel={this.handleCancel.bind(this)} />
                    </div>
                </div>
                }
            </div>
        </div>

    }

    courseClassHeader = () => {
        let item = this.state.data;
        let startAt = moment(item.startAt).format("YYYY.MM.DD HH:mm");
        let endAt = moment(item.endAt).format("YYYY.MM.DD HH:mm");
        const  {shortKnowledges,knowledges} = item;
        return <div className='course-class-header'>
            <div className='left' onClick={this.playVideo.bind(this,item)}>
                <img className='cover' src={item.coverUrl} alt='' />
                <div className='gray-bg'></div>
                <img className='play-img' src={require('../../static/img/play-btn.png')} alt=''/>
            </div>
            <div className='center'>
                <div className='course-name'>
                    <span>第{item.number}课时</span><span style={{marginLeft:'10px'}}>{item.title}</span>
                </div>
                <div className='course-time'>在线辅导时间：{startAt + '  -  ' + endAt}</div>
                <div className='course-title'>课程知识点：</div>
                <div className='course-content' dangerouslySetInnerHTML={{ __html: shortKnowledges}}></div>
                <div className='courseware' onClick={this.checkFullCourseware.bind(this,item)}>查看完整课件>></div>
            </div>
            <Button type="primary" className="student-leave-message" onClick={this.studentLeaveMessage.bind(this,item)}>查看学生留言</Button>
        </div>
    }
    lessonDetailFetch = ()=>{

        let loginToken =  localStorage.getItem(LOGINTOKEN);
        let courseId = this.props.match.params.courseId;
        let teacherCourseId = this.props.match.params.teacherCourseId;
        let courseLessonId = this.props.match.params.courseLessonId;

        this.setState({
            isLoading:true,
        })
        courseLessonDetailFetch(loginToken,courseId,teacherCourseId,courseLessonId).then((result)=>{
            console.log('courseLessonDetailFetch',result.data);
            if (result.data && result.data.code === 200)
            {
                let data = result.data.data;

                // let aTemp = result.data.data.studentCourseHomeworkInfoList[0];
                // for (var i=0;i<10;i++)
                // {
                //     var newObj = JSON.parse(JSON.stringify(aTemp));
                //
                //     if (i%2 === 0)
                //     {
                //         newObj.onlineFlag = 2;
                //         newObj.state = 2;
                //     }
                //     else {
                //         newObj.onlineFlag = 1;
                //         newObj.state = 1;
                //     }
                //     newObj.comment = newObj.comment + i + 'p';
                //     data.studentCourseHomeworkInfoList.push(newObj);
                // }

                this.setState({
                    data:data,
                    isLoading:false,
                })
            }
            else
            {
                message.warning(result.data.error);
                this.setState({
                    isLoading:false,
                })
            }

        }).catch((error)=>{
            this.setState({
                isLoading:false,
            })
        })

    }
     commentFetch =(textValue)=>{

        console.log('comment',textValue);
        if (!textValue||textValue.length ===0)
        {
            message.warning('请填写评论内容');
            return;
        }
         let loginToken =  localStorage.getItem(LOGINTOKEN);
         let studentCourseHomeworkId = this.state.currentItem.id;
         homeworkCommentFetch(loginToken,studentCourseHomeworkId,textValue).then((result)=>{
             console.log('result',result.data);
             if (result.data && result.data.code === 200)
             {
                 let studentCourseHomeworkInfoList = this.state.data.studentCourseHomeworkInfoList;
                 for (let i=0;i<studentCourseHomeworkInfoList.length;i++)
                 {
                     let tempItem = studentCourseHomeworkInfoList[i];
                     if (tempItem.id === this.state.currentItem.id)
                     {
                         tempItem.comment = textValue;
                     }
                 }
                 this.setState({
                     commentsVisible:false,
                 })
             }
             else {
                 message.warning(result.data.error)
             }

         }).catch((error)=>{

         })

    }
    //点击播放
    playVideo= ()=>{
        console.log('点击播放');
        let item = this.state.data;
        this.setState({
            visible: true,
            currentVideoUrl:item.videoUrl,
        });
    }
    handleCancel = (e) => {
        document.body.style.overflow = 'auto'
        this.setState({
            visible: false,
        });
    }
    //查看学生留言
    studentLeaveMessage = ()=>{
        let titles = localStorage.getItem('courseTitle');
        titles = JSON.parse(titles);
        titles.type = 3;
        titles.title3 = '课堂';
        titles = JSON.stringify(titles);
        localStorage.setItem("courseTitle",titles);

        let courseId = this.props.match.params.courseId;
        let teacherCourseId = this.props.match.params.teacherCourseId;
        let courseLessonId = this.props.match.params.courseLessonId;
        this.props.history.push(`/student-leavemessage/${courseId}/${teacherCourseId}/${courseLessonId}`);
    }
    //查看完整课件
    checkFullCourseware = ()=>{
        let titles = localStorage.getItem('courseTitle');
        titles = JSON.parse(titles);
        titles.type = 3;
        titles.title3 = '课堂';
        titles = JSON.stringify(titles);
        localStorage.setItem("courseTitle",titles);
        this.props.history.push(`/full-courseware/`);

    }

    studentList = ()=>{
        if (this.state.data.studentCourseHomeworkInfoList){
            return <div className='student-list'>
                {
                    this.state.data.studentCourseHomeworkInfoList.map((item,index)=>{

                        let  studentAvatarUrl = item.studentAvatarUrl;
                        let coverUrl = item.coverUrl;
                        let studentName = item.studentName;
                        let onlineFlag = item.onlineFlag; // 1 离线 2 在线
                        let state = item.state; // 状态(1未保存、2未提交、3已提交)
                        if(this.state.allOnline &&  onlineFlag === 1)
                        {
                            return [];
                        }
                        const  {comment} = item;
                        return <div key={index} className='student-list-row'>
                            {
                                state === 3 ?
                                    <div className='top'>
                                        <img className='cover' src={coverUrl} alt=''/>
                                        {/*<div className='cover-mask'></div>*/}
                                        <div className='check-works' onClick={this.checkWorks.bind(this,item)}>查看作品</div>
                                        {
                                            comment&&comment.length?   <div className='comments' onClick={this.checkComments.bind(this,item)}>
                                                <img className='comments-img'  src={require('../../static/img/commit.png')} alt=''/>
                                                <span className='comments-title'>评语</span> </div>:''
                                        }
                                        {/*<div className='public-state'>已提交</div>*/}
                                    </div>
                                    :
                                    <div className='top'>
                                        <img className='cover' src={coverUrl} alt=''/>
                                        <div className='cover-mask'></div>
                                        <div className='no-public-state'>未提交</div>
                                </div>
                            }
                            <div className='bottom'>
                                <img className='head-portrait' src={studentAvatarUrl} alt=''/>
                                <div className='name'>{studentName}</div>
                                {onlineFlag === 2?
                                    <div className='works-state'>
                                        <span className='public'>上线</span>
                                    </div>
                                    : <div className='works-state'>
                                        <span className='no-public'>未上线</span>
                                        <img className='warning-tip'  src={require('../../static/img/warning-tip.png')} alt=''/>
                                    </div>
                                }

                            </div>
                        </div>
                    })
                }
            </div>
        }
    }
    checkOnline = ()=>{
        this.setState({
            allOnline:!this.state.allOnline,
        })
    }
    checkComments = (item)=>{
        this.setState({
            commentsVisible:true,
            currentItem:item,
        })
    }
    checkWorks = (item)=>{
        const {id} = item;
        window.open(`${jumpApi}?type=2&studentCourseHomeworkId=${id}`);
    }


}
