
import React from 'react'
import '../../app.less'
import './index.less'

import {Input, message,Pagination,Spin} from 'antd';
import Header from "../../components/header";
import Content from "../../components/content";
import {courseListFetch} from "../../service/courseFetch";
import CourseListView from "../../components/courseListView";
import GeneralEmpty from "../../components/generalEmpty";
import {LOGINTOKEN} from "../../utils/constants";


export  default  class CourseManagement  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            isLoading:false,
            onGoing:{},
            toStart:{},
            endData: {},

            searchValue:null,
            totalSize:0,
            searchData:[],
            searchPage:0,
        }
        console.log('CourseManagement constructor ',this);
    }
    componentDidMount(){
        console.log('CourseManagement componentDidMount ');
        this.listFetch();
    }
    componentWillReceiveProps(nextProps,prevState){
        console.log('CourseManagement componentWillReceiveProps ',nextProps,prevState);

    }
    render(){

        return <div>
            <Header type={4} searchKeywordClick={this.searchKeywordClick.bind(this)}/>
            <Content>
                <div style={{
                paddingBottom:'40px'
                }}>
                    <div className='management-root-bg'>
                        {this.showBody()}
                    </div>
                </div>
            </Content>
        </div>
    }
    showBody =()=>{
        const {onGoing,toStart,endData} = this.state;

        if (this.state.isLoading)
        {
            return <Spin size="large" style={{"fontSize": "30px", "display": 'block', 'margin': '380px auto'}}/>;
        }
        if (this.state.searchValue&&this.state.searchValue.length)
        {
            if (!this.state.searchData.length)
            {
                return <GeneralEmpty hintText={'暂无搜索到课程'}/>
            }
            return <div className='search-list-bg'>
                <CourseListView courseList={this.courseList.bind(this)} data={this.state.searchData}/>
                <div className='pagination'>
                    {
                        this.state.totalSize > 12 ? <Pagination pageSize={12}
                                                                className='pagination-p'
                                                                total={this.state.totalSize}
                                                                hideOnSinglePage={true}
                                                                onChange={this.onChangePage.bind(this)}/> : ""
                    }
                </div>

            </div>
        }
        if (!onGoing.count && !toStart.count && !endData.count)
        {
            return  <GeneralEmpty hintText = {'暂无课程'}/>
        }
        return  <div className='management-root'>
            {this.responsibleCourse.bind(this)()}
            {this.toStartCourse.bind(this)()}
            {this.endCourse.bind(this)()}
        </div>
    }
     listFetch = ()=>{
        console.log('listFetch this =>',this);
         let loginToken =  localStorage.getItem(LOGINTOKEN);
         if (loginToken&& loginToken.length)
         {
             this.setState({
                 isLoading:true,
             })
             courseListFetch(loginToken,0,null,0).then((result)=>{
                 console.log('courseListFetch this',this,'\n result',result);
                 if (result.data && result.data.code === 200)
                 {
                     let data = result.data.data;
                     let onGoing = {};
                     let toStart = {};
                     let endData = {};

                     for (let value  of data)
                     {
                         if (value.type === '2' && value.count)
                         {
                             onGoing = value;
                             // for (let i=0;i<16;i++)
                             // {
                             //     let tempItem =  onGoing.list[0];
                             //     var newObj = JSON.parse(JSON.stringify(tempItem));
                             //     onGoing.list.push(newObj);
                             // }
                         }
                         if (value.type === '1' && value.count)
                         {
                             toStart = value;
                         }
                         if (value.type === '3' && value.count)
                         {
                             endData = value;
                             // for (let i=0;i<16;i++)
                             // {
                             //     let tempItem =  endData.list[0];
                             //     var newObj = JSON.parse(JSON.stringify(tempItem));
                             //     endData.list.push(newObj);
                             // }
                         }
                     }
                     //

                     this.setState({
                         onGoing:onGoing,
                         toStart:toStart,
                         endData:endData,
                         isLoading:false,
                     })
                 }
                 else
                 {
                     message.warning(result.data.error);
                     this.setState({
                         isLoading:false,
                     })
                 }

             }).catch((error)=>{
                 console.log('error',error.message);
                 this.setState({
                     isLoading:false,
                 })
             })
             // courseListFetch(loginToken,2,null,0,1000).then((result)=>{
             //     console.log('courseListFetch this',this,'\n result',result);
             //     if (result.data && result.data.code === 200)
             //     {
             //         let data = result.data.data.content;
             //         // for (let i=0;i<16;i++)
             //         // {
             //         //     let tempItem = result.data.data.content[0];
             //         //     var newObj = JSON.parse(JSON.stringify(tempItem));
             //         //     data.push(newObj);
             //         // }
             //         this.setState({
             //             userManageData:data,
             //             isLoading:false,
             //         })
             //     }
             //     else
             //     {
             //         message.warning(result.data.error);
             //         this.setState({
             //             isLoading:false,
             //         })
             //     }
             //
             // }).catch((error)=>{
             //     console.log('error',error.message);
             //     this.setState({
             //         isLoading:false,
             //     })
             // })
             // courseListFetch(loginToken,3,null,0,8).then((result)=>{
             //     console.log('courseListFetch this',this,'\n result',result);
             //     if (result.data && result.data.code === 200)
             //     {
             //         let data = result.data.data.content;
             //         // for (let i=0;i<16;i++)
             //         // {
             //         //     let tempItem = result.data.data.content[0];
             //         //     var newObj = JSON.parse(JSON.stringify(tempItem));
             //         //     data.push(newObj);
             //         // }
             //         this.setState({
             //             isLoading:false,
             //             endData:data,
             //             endDataTotalSize:result.data.data.pageable.totalSize
             //         })
             //     }
             //     else
             //     {
             //         message.warning(result.data.error);
             //         this.setState({
             //             isLoading:false,
             //         })
             //     }
             //
             // }).catch((error)=>{
             //     this.setState({
             //         isLoading:false,
             //     })
             //     console.log('error',error.message);
             // })
         }
     }
    searchKeywordClick=(searchValue)=>{
        console.log('searchKeywordClick',searchValue);
        this.setState({
            searchValue:searchValue,
        });
        if (searchValue&&searchValue.length)
        {
            this.state.searchPage = 0;
            this.searchFetch(searchValue);
        }
        else
        {
            this.listFetch();
        }
    }
    searchFetch = (value)=>{
        let loginToken =  localStorage.getItem(LOGINTOKEN);
        this.setState({
            searchData:[],
            totalSize:0,
            isLoading:true,
        })
        courseListFetch(loginToken,null,value,this.state.searchPage,12).then((result)=>{
            console.log('courseListFetch this',this,'\n result',result);
            if (result.data && result.data.code === 200)
            {
                let data = result.data.data.content;
                // for (let i=0;i<10;i++)
                // {
                //     let tempItem = result.data.data.content[0];
                //     var newObj = JSON.parse(JSON.stringify(tempItem));
                //     data.push(newObj);
                // }
                this.setState({
                    searchData:data,
                    totalSize:result.data.data.pageable.totalSize,
                    isLoading:false,
                })
            }
            else
            {
                message.warning(result.data.error);
                this.setState({
                    isLoading:false,
                })
            }

        }).catch((error)=>{
            this.setState({
                isLoading:false,
            })
            console.log('error',error.message);
        })
    }
    responsibleCourse=()=>{
        const {list = [],count,type} = this.state.onGoing;
        if (list.length) {
            let maxCount = 8;
            let showMore = count>maxCount;
            let data = list.length>maxCount?list.slice(0,maxCount):list;
            return  <div  className='user-manage-course-bg'>
                <div className='user-manage-course-title'>进行中课程</div>
                <div className='tips'></div>
                {
                    showMore?<div className='check-more-course' onClick={this.checkMoreCoure.bind(this,type)}>查看更多>></div>:''
                }
                <CourseListView courseList={this.courseList.bind(this)} data={data}/>
            </div>
        }

    }
    toStartCourse=()=>{
        const {list = [],count,type} = this.state.toStart;
        if (list.length) {
            let maxCount = 8;
            let showMore = count>maxCount;
            let data = list.length>maxCount?list.slice(0,maxCount):list;
            return  <div  className='user-manage-course-bg'>
                <div className='user-manage-course-title'>待开始课程</div>
                <div className='tips'></div>
                {
                    showMore?<div className='check-more-course' onClick={this.checkMoreCoure.bind(this,type)}>查看更多>></div>:''
                }
                <CourseListView courseList={this.courseList.bind(this)} data={data}/>
            </div>
        }

    }
    endCourse=()=>{
        const {list = [],count,type} = this.state.endData;
        if (list.length) {
            let maxCount = 8;
            let showMore = count>maxCount;
            let data = list.length>maxCount?list.slice(0,maxCount):list;
            return  <div  className='user-manage-course-bg'>
                <div className='user-manage-course-title'>已完结课程</div>
                <div className='tips'></div>
                {
                    showMore?<div className='check-more-course' onClick={this.checkMoreCoure.bind(this,type)}>查看更多>></div>:''
                }
               <CourseListView courseList={this.courseList.bind(this)} data={data}/>
            </div>
        }
    }
    courseList = (item)=>{
        console.log('courseList',item);
        var titles = {title1:item.name,title2:item.issue+'期'};
        titles = JSON.stringify(titles);
        localStorage.setItem("courseTitle",titles);
        var path = `/course-lesson-list/${item.id}/${item.teacherCourseId}`;
        this.props.history.push(path);

    }
    checkMoreCoure = (type)=>{
        console.log('查看更多');
        this.props.history.push(`/check-morecourse/${type}`)
    }
    onChangePage = (page, pageSize)=>{
        console.log('onChangePage',page,pageSize);
        this.state.searchPage = page-1;
        if (this.state.searchValue&&this.state.searchValue.length)
        {
            this.searchFetch(this.state.searchValue);
        }
    }

}
