
/**
 * 学生作业
 * */
import React from 'react'
import './index.less'
import Header from "../../components/header";
import Content from "../../components/content";
import {courseLessonDetailFetch, homeworkCommentFetch} from "../../service/courseFetch";
import {message,Modal,Spin} from "antd";
import CommentsPop from "../../components/commentsPop";
import {jumpApi, LOGINTOKEN} from "../../utils/constants";
import GeneralEmpty from "../../components/generalEmpty";

export  default  class studentWork  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            data:{},

        };

        console.log('studentWork constructor ');

    }
    componentWillMount() {
        console.log('studentWork componentWillMount ');
    }
    componentDidMount(){
        console.log('studentWork componentDidMount ');
        this.lessonDetailFetch();
    }
    componentWillReceiveProps(nextProps){
        console.log('studentWork componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('studentWork \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){
        let titles = localStorage.getItem('courseTitle');
        titles = JSON.parse(titles);
        let title1 = titles.title1;
        let title2 = titles.title2;
        let title3 = titles.title3;
        let title4 = '学生作业';

        return <div>
            <Header type={2} hiddenCreation={true} title1={title1} title2 = {title2} title3={title3} title4={title4}/>
            <Content>
                {this.showBody.bind(this)()}
                <Modal
                    visible={this.state.commentsVisible}
                    footer={null}
                    closable={false}
                    width='646px'
                    bodyStyle={{
                        padding:'0px',
                    }}
                    onOk={()=>{
                        this.setState({
                            commentsVisible:false,
                        })
                    }}
                    onCancel={()=>{
                        this.setState({
                            commentsVisible:false,
                        })
                    }}
                >
                    <CommentsPop
                        studentName={this.state.currentItem&&this.state.currentItem.studentName}
                        textValue={this.state.currentItem&&this.state.currentItem.comment}
                        onCancelPop={()=>{
                            this.setState({
                                commentsVisible:false,
                            })
                        }}
                        onOkPop={(textValue)=>{
                            this.commentFetch.bind(this,textValue)();
                        }}
                    />

                </Modal>
            </Content>
        </div>
    }

    showBody = ()=>{
        const { data:{commitCount = 0}} = this.state;

        if (this.state.isLoading){

            return <Spin size="large" style={{"fontSize":"30px","display":'block','margin':'320px auto'}}/>;
        }
        return  <div className='works-root'>
            <div className='student-works-root'>
                <div className='student-works-title'>
                    <img className='img-tip' src={require('../../static/img/person.png')}/>
                    <span className='title'>全部学员</span>
                    <span className="submitted-quantity">已提交数量：{commitCount?commitCount:0}</span>
                </div>
                {this.studentList.bind(this)()}
            </div>
        </div>
    }

    studentList = ()=>{
        const { data:{studentCourseHomeworkInfoList = []}} = this.state;
        if (!studentCourseHomeworkInfoList.length)
        {

            return <GeneralEmpty  hintText = '暂无学员'/>
        }
        if (studentCourseHomeworkInfoList.length){
            return <div className='student-list'>
                {
                    this.state.data.studentCourseHomeworkInfoList.map((item,index)=>{

                        let  studentAvatarUrl = item.studentAvatarUrl;
                        let coverUrl = item.coverUrl;
                        let studentName = item.studentName;
                        let onlineFlag = item.onlineFlag; // 1 离线 2 在线
                        let studyFlag = item.studyFlag; // 学习标识(1未学习、2已学习)
                        let state = item.state; // 状态(1未保存、2未提交、3已提交)
                        const {comment} = item;
                        console.log('comment&&comment.length',comment,comment&&comment.length)

                        return <div key={index} className='student-list-row'>
                            {
                                state === 3 ?
                                    <div className='top'>
                                        <img className='cover' src={coverUrl}/>
                                        <div className='check-works' onClick={this.checkWorks.bind(this,item)}>查看作品</div>
                                        {
                                            comment&&comment.length?  <div className='comments' onClick={this.checkComments.bind(this,item)}>
                                                <img className='comments-img'  src={require('../../static/img/commit.png')}/>
                                                <span className='comments-title'>评语</span>
                                            </div>:''
                                        }
                                        {/*<div className='public-state'>已提交</div>*/}
                                    </div>
                                    :
                                    <div className='top'>
                                        <img className='cover' src={coverUrl}/>
                                        <div className='cover-mask'></div>
                                        <div className='no-public-state'>未提交</div>
                                    </div>
                            }
                            <div className='bottom'>
                                <img className='head-portrait' src={studentAvatarUrl}/>
                                <div className='name'>{studentName}</div>
                                {studyFlag === 2?
                                    <div className='works-state'>
                                        {/*<span className='public'>上线</span>*/}
                                    </div>
                                    : <div className='works-state'>
                                        <span className='no-public'>未上线</span>
                                        <img className='warning-tip'  src={require('../../static/img/warning-tip.png')}/>
                                    </div>
                                }

                            </div>
                        </div>
                    })
                }
            </div>
        }
    }
    lessonDetailFetch = ()=>{

        let loginToken =  localStorage.getItem(LOGINTOKEN);
        let courseId = this.props.match.params.courseId;
        let teacherCourseId = this.props.match.params.teacherCourseId;
        let courseLessonId = this.props.match.params.courseLessonId;

        this.setState({
            isLoading:true,
        })
        courseLessonDetailFetch(loginToken,courseId,teacherCourseId,courseLessonId).then((result)=>{
            console.log('courseLessonDetailFetch',result.data);
            if (result.data && result.data.code === 200)
            {
                let data = result.data.data;
                //
                // let aTemp = result.data.data.studentCourseHomeworkInfoList[0];
                // for (var i=0;i<20;i++)
                // {
                //     var newObj = JSON.parse(JSON.stringify(aTemp));
                //
                //     if (i%2 === 0)
                //     {
                //         newObj.studyFlag = 2;
                //         newObj.state = 2;
                //     }
                //     else {
                //         newObj.studyFlag = 1;
                //         newObj.state = 1;
                //     }
                //     newObj.comment = newObj.comment + i + 'p';
                //     data.studentCourseHomeworkInfoList.push(newObj);
                // }

                this.setState({
                    data:data,
                    isLoading:false,
                })
            }
            else
            {
                message.warning(result.data.error);
                this.setState({
                    isLoading:false,
                })
            }

        }).catch((error)=>{
            this.setState({
                isLoading:false,
            })
        })

    }
    commentFetch =(textValue)=>{

        console.log('comment',textValue);
        if (!textValue||textValue.length ===0)
        {
            message.warning('请填写评论内容');
            return;
        }
        let loginToken =  localStorage.getItem(LOGINTOKEN);
        let studentCourseHomeworkId = this.state.currentItem.id;
        homeworkCommentFetch(loginToken,studentCourseHomeworkId,textValue).then((result)=>{
            console.log('result',result.data);
            if (result.data && result.data.code === 200)
            {
                let studentCourseHomeworkInfoList = this.state.data.studentCourseHomeworkInfoList;
                for (let i=0;i<studentCourseHomeworkInfoList.length;i++)
                {
                    let tempItem = studentCourseHomeworkInfoList[i];
                    if (tempItem.id === this.state.currentItem.id)
                    {
                        tempItem.comment = textValue;
                    }
                }
                this.setState({
                    commentsVisible:false,
                })
            }
            else {
                message.warning(result.data.error)
            }

        }).catch((error)=>{

        })

    }
    checkComments = (item)=>{
        this.setState({
            commentsVisible:true,
            currentItem:item,
        })
    }
    checkWorks = (item)=>{
        console.log('item',item);
        const {id} = item;
        //http://192.168.0.107:8601/?type=2&studentCourseHomeworkId=1
        window.open(`${jumpApi}?type=2&studentCourseHomeworkId=${id}`);
    }

}
