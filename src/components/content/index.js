
/**
 * 通用content
 * */
import React from 'react'
import './index.less'


export  default  class Content  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {

        };
        console.log('Content constructor');
    }
    componentWillMount() {
        console.log('Content componentWillMount ');
    }
    componentDidMount(){
        console.log('Content componentDidMount ');
    }
    componentWillReceiveProps(nextProps){
        console.log('Content componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('Content \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){

        return <div className='content-root'>
            {this.props.children}
        </div>
    }

}
