
/*获取地址栏参数方法*/
export const GetSearchQueryString = (name)=>{
    console.log('地址',window.location)
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    let r = window.location.search.substr(1).match(reg);
    if(r!=null)return  decodeURI(r[2]); return null;
}
/*获取 href 地址栏参数方法*/
export const GetHrefQueryString = (name)=>{
    console.log('地址',window.location)
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.href.split('?')[1]&&window.location.href.split('?')[1].match(reg);
    if(r!=null)return  decodeURI(r[2]); return null;
}
//参数值不为空时，存在参数则更新，否则不操作
export const updateQueryStringParameter = (uri, key, value)=> {
    if(!value) {
        return uri;
    }
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}
