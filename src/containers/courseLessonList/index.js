
/**
 * 课程 小结课时 列表
 * */
import React from 'react'
import './index.less'
import Header from "../../components/header";
import Content from "../../components/content";
import { Button, message,Spin,Icon} from 'antd';
import {courseLessonInfoListFetch} from "../../service/courseFetch";
import moment from "moment/moment";
import GeneralEmpty from "../../components/generalEmpty";
import Myvideo from '../../components/Myvideo/Myvideo.js'
import {LOGINTOKEN} from "../../utils/constants";

export  default  class CourseLessonList  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            data:{},
            selectedIndex:0,
            isLoading:false,
            visible:false,
        };

        console.log('CourseList constructor ');

    }
    componentWillMount() {
        console.log('CourseList componentWillMount ');
    }
    componentDidMount(){
        console.log('CourseList componentDidMount ');
        this.listFetch();
    }
    componentWillReceiveProps(nextProps){
        console.log('CourseList componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('CourseList \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){

         let titles = localStorage.getItem('courseTitle');
        titles = JSON.parse(titles);
        let title1 = titles.title1;
        let title2 = titles.title2;

        let barArray = [
            {
                title: '课程内容',
            },
            {
                title: '学员列表',
            }
        ]
        return <div>
            <Header hiddenCreation={true} type={1} title1={title1} title2 = {title2}/>
            <Content >
                <div style={{
                    paddingBottom:'40px',
                }}>
                    <div className='management-lesson-root-bg'>
                        <div className='header-bar'>
                            {
                                barArray.map((item,index)=>{

                                    let style ={marginLeft:'35px'};
                                    if (index===0)
                                    {
                                        style ={marginLeft:'45px'};
                                    }
                                    let titleClass = this.state.selectedIndex === index?'title select':'title';
                                    return <div className='item' key={index} style={style} onClick={()=>{
                                        this.setState({
                                            selectedIndex:index,
                                        },()=>{
                                            // this.listFetch.bind(this)();
                                        })
                                    }}>
                                        <div className={titleClass}>
                                            {`${item.title}`}
                                        </div>
                                        {this.state.selectedIndex === index? <div className='bottom-line'></div>:''}
                                    </div>
                                })
                            }
                        </div>
                        {this.showList.bind(this)()}
                    </div>
                </div>
            </Content>
            </div>
    }
    showList=()=> {

        const {isLoading,visible, currentVideoUrl, selectedIndex,data:{courseLessonInfoList=[],studentInfoList=[]}} = this.state;

        if (isLoading) {
            return <Spin size="large" style={{"fontSize": "30px", "display": 'block', 'margin': '380px auto'}}/>;
        }
        if (selectedIndex === 0) {

            if (courseLessonInfoList.length === 0)
            {
                return <GeneralEmpty  hintText = '暂无课程'/>
            }
            return <div className='course-list-root'>
                {
                    courseLessonInfoList.map((item, index) => {

                        let startAt = moment(item.startAt).format("YYYY.MM.DD HH:mm");
                        let endAt = moment(item.endAt).format("YYYY.MM.DD HH:mm");
                        const  {shortKnowledges,knowledges} = item;
                        return <div key={index}>
                            <div className='course-list-row'>
                                <div className='left' onClick={this.playVideo.bind(this, item)}>
                                    <img className='cover' src={item.coverUrl}/>
                                    <div className='gray-bg'></div>
                                    <img className='play-img' src={require('../../static/img/play-btn.png')}/>
                                </div>
                                <div className='center'>
                                    <div className='course-name'>
                                        <span>第{item.number}课时</span><span
                                        style={{marginLeft: '10px'}}>{item.title}</span>
                                    </div>
                                    <div className='course-time'>在线辅导时间：{startAt + ' - ' + endAt}</div>
                                    <div className='course-title'>课程知识点：</div>
                                    <div className='course-content' dangerouslySetInnerHTML={{ __html: shortKnowledges}}></div>
                                    <div className='courseware'
                                         onClick={this.checkFullCourseware.bind(this, item)}>查看完整课件>>
                                    </div>
                                </div>
                                {this.showStateContent(item)}
                            </div>
                            {index !== courseLessonInfoList.length - 1 ? <div className='bottom-line'></div> : ''}
                        </div>

                    })
                }
                {visible && <div onClick={this.handleCancel} className='show-background'>
                    <div onClick={(e) => {
                        e.stopPropagation()
                    }} className='show-video-content'>
                        <Myvideo videoUrl={currentVideoUrl} handleCancel={this.handleCancel.bind(this)}/>
                    </div>
                </div>
                }
            </div>
        }
        else {

            if (studentInfoList.length === 0) {
                return <GeneralEmpty hintText='暂无学生'/>
            }
            return <div className='student-list-root'>
                <div className='student-header-bar'>
                    <div className='student-header-number'>序号</div>
                    <div className='student-header-name'>姓名</div>
                    <div className='student-header-phone'>手机号</div>
                </div>
                {
                    studentInfoList.map((item, index) => {
                        const {name,phone,avatarUrl} = item;
                        return <div className={index%2?'student-row student-row-select':'student-row'}>
                            <div className='student-number'>{index + 1}</div>
                            <div className='student-name'>
                                <img className='img' src={avatarUrl}/>
                                <div className='name'>{name}</div>
                            </div>
                            <div className='student-phone'>{phone}</div>
                        </div>
                    })
                }
            </div>
        }
    }

    showStateContent = (item)=>{

        let currentTime = new Date().getTime();
        let endAt = item.endAt;
        let startAt = item.startAt;

        if (startAt>currentTime) {
            return <div>
                <div  className="stay-open-title">待开放</div>
                <img  className="stay-open-img"  src={require('../../static/img/stay-open-img.png')}/>
            </div>
        }
        else if(startAt<currentTime && endAt>currentTime)
        {
            return <Button type="primary" className="go-course" onClick={this.classRoom.bind(this, item)}>进入课堂</Button>
        }
        else if(currentTime>endAt) {

            return <div>
                <div  className="student-work" onClick={this.studentWork.bind(this,item)}>学生作业</div>
                {item.unCheckCount ? <div className="uncheckCount">{item.unCheckCount}</div>:''}
                <div  className="student-leave-message" onClick={this.studentLeaveMessage.bind(this,item)}>查看学生留言</div>
                {item.messageCount? <div className="message-count">{item.messageCount}</div>:''}
            </div>
        }

    }
    listFetch = ()=>{

        let loginToken =  localStorage.getItem(LOGINTOKEN);
        let courseId = this.props.match.params.courseId;
        let teacherCourseId = this.props.match.params.teacherCourseId;
        console.log('listFetch params ',this.props.match.params);
        this.setState({
            isLoading:true,
        })
        courseLessonInfoListFetch(loginToken,courseId,teacherCourseId).then((result)=>{

            console.log('result',result.data);
            if (result.data && result.data.code === 200)
            {

                let data = result.data.data;//slice(0,1);
                // for (let i = 0; i<data.length; i++)
                // {
                    // let value = data[i];
                    // value.endAt = value.endAt + 900000000;
                    // value.startAt = value.startAt + 900000000;
                    // value.endAt = 1559732400000;
                    // value.startAt = 1559707200000;
                // }
                // for (let i=0;i<10;i++)
                // {
                //     let tempItem = result.data.data.courseLessonInfoList[0];
                //     var newObj = JSON.parse(JSON.stringify(tempItem));
                //     data.courseLessonInfoList.push(newObj);
                // }
                // for (let i=0;i<50;i++)
                // {
                //     let tempItem = result.data.data.studentInfoList[0];
                //     var newObj = JSON.parse(JSON.stringify(tempItem));
                //     data.studentInfoList.push(newObj);
                // }
                this.setState({
                    data:data,
                    isLoading:false,
                })
            }
            else
            {
                message.warning(result.data.error);
                this.setState({
                    isLoading:false,
                })
            }

        }).catch(()=>{
            this.setState({
                isLoading:false,
            })
        })
    }
    //点击播放
    playVideo= (item)=>{
        console.log('点击播放',item);
        this.setState({
            visible: true,
            currentVideoUrl:item.videoUrl,
        });
    }
    handleCancel = (e) => {
        document.body.style.overflow = 'auto'
        this.setState({
            visible: false,
        });
    }

    //进入课堂
    classRoom = (item)=>{
        let courseId = this.props.match.params.courseId;
        let teacherCourseId = this.props.match.params.teacherCourseId;
        this.props.history.push(`/class-room/${courseId}/${teacherCourseId}/${item.id}`);

    }
    //学生作业
    studentWork = (item)=>{

        let titles = localStorage.getItem('courseTitle');
        titles = JSON.parse(titles);
        titles.type = 2;
        titles.title3 = '第'+item.number+'课时';
        titles = JSON.stringify(titles);
        localStorage.setItem("courseTitle",titles);

        let courseId = this.props.match.params.courseId;
        let teacherCourseId = this.props.match.params.teacherCourseId;
        this.props.history.push(`/student-work/${courseId}/${teacherCourseId}/${item.id}`);

    }
    //查看学生留言
    studentLeaveMessage = (item)=>{

        let titles = localStorage.getItem('courseTitle');
        titles = JSON.parse(titles);
        titles.type = 2;
        titles.title3 = '第'+item.number+'课时';
        titles = JSON.stringify(titles);
        localStorage.setItem("courseTitle",titles);

        let courseId = this.props.match.params.courseId;
        let teacherCourseId = this.props.match.params.teacherCourseId;
        this.props.history.push(`/student-leavemessage/${courseId}/${teacherCourseId}/${item.id}`);
    }
    //查看完整课件
    checkFullCourseware = (item)=>{

        let titles = localStorage.getItem('courseTitle');
        titles = JSON.parse(titles);
        titles.type = 2;
        titles.title3 = '第'+item.number+'课时';
        titles = JSON.stringify(titles);
        localStorage.setItem("courseTitle",titles);

        let docUrl = item.docUrl;
        localStorage.setItem('courseware',docUrl);
        this.props.history.push('/full-courseware/');


    }


}
