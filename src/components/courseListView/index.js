

/**
 * 课程 userManageCourse
 * */
import React from 'react'
import './index.less'


export  default  class CourseListView  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {

        };

        console.log('CourseListView constructor ');

    }
    componentWillMount() {
        console.log('CourseListView componentWillMount ');
    }
    componentDidMount(){
        console.log('CourseListView componentDidMount ');
    }
    componentWillReceiveProps(nextProps){
        console.log('CourseListView componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('CourseListView \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){

        return  <div className='user-manage-course'>
            {
                this.props.data&&this.props.data.map((item,index)=>{
                    let src = item.coverUrl;
                    return <div key={index} className='course' onClick={this.courseList.bind(this,item)}>
                        <div className='top'>
                            <img className='course-img' src={src} alt={'图片'}/>
                            <div className='issue'>{item.issue}期</div>
                            <div className='coursePackageName'>{item.coursePackageName}</div>
                        </div>
                        <div className='bottom'>{item.name}</div>
                    </div>
                })
            }
        </div>
    }
    courseList = (item)=>{
        if (this.props.courseList) {
            this.props.courseList(item);
        }
    }

}
