import React from 'react'
import '../login/index.less'

import {Button, message} from 'antd';
import {saveLoginInfo} from "../../redux/actions/loginactions";
import {connect} from "react-redux";
import {GetSearchQueryString, GetHrefQueryString} from "../../utils/commenutil";
import {teacherInfor, weixinLogin} from "../../service/personFetch";
import {LOGINTOKEN, USERINFO, weixinRedirectUri,appId,appKey,accountType} from "../../utils/constants";

@connect()
class Login1 extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            showWeixin: false,
        };

        console.log('Login constructor', this.props);

    }

    componentWillMount() {
        console.log('Login componentWillMount ');
        this.getWeixinCode.bind(this)();
    }

    componentDidMount() {
        console.log('Login componentDidMount ');
    }

    componentWillReceiveProps(nextProps) {
        console.log('Login componentWillReceiveProps', nextProps);
    }

    componentDidUpdate(preProps) {
        console.log('Login preProps', preProps);
        // this.getWeixinCode.bind(this)();
    }

    login = () => {
        // localStorage.setItem("loginLoading", '1');
        // this.setState({
        //     showWeixin:true,
        // })
        // setTimeout(()=>{
        //      window.location.href='http://localhost:3000/#/login?code=0219SC9s0rq0hf1qoV7s0V2G9s09SC9m';
        // },3000);
        //红涛账号
        // let ssoId = 20111594;
        // let oauthId = 'og_h10rR1Tc_7VMXMbxVj7lXGNdo';
        // let avatarUrl = 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoLG6lTcYjVo7Zf1FVXf5ypj7fLK1BAAkwgTO5NzXnkmWGvSyicqVgg1k7ytCUCYUm6UzSlt9TeCQg/132';
        // let nickname = '李红涛';
        // this.props.history.push(`/bind-phone?ssoId=${ssoId}&oauthId=${oauthId}&&avatarUrl=${avatarUrl}&name=${nickname}`);
        // return;

        localStorage.setItem("loginLoading", '1');
        window.location.href = `https://open.weixin.qq.com/connect/qrconnect?appid=wx41ddc056b87016ba&redirect_uri=${weixinRedirectUri}&response_type=code&scope=snsapi_login#wechat_redirect`;
    }
    getWeixinCode = () => {
        let loginLoading = localStorage.getItem("loginLoading");
        console.log('loginLoading',loginLoading);
        if (parseInt(loginLoading)) {
            localStorage.setItem("loginLoading", '0');
            let code1 = GetSearchQueryString('code');
            let code2 = GetHrefQueryString('code');
            console.log(' search code:', code1);
            console.log(' href code:', code2);
            // console.log('this.URL', document.URL,window.location,window.location.origin,this.props.location);
            // let params = new URLSearchParams(window.location.search);
            // console.log('window URLSearchParams url:', params.toString());

            let params2 = new URLSearchParams(this.props.location.search);
            let code3 = params2.get('code');
            console.log('props URLSearchParams2 code:', code3);

            let code = code3;
            if (code != null) {
                weixinLogin(appId, appKey, accountType, code).then((result) => {
                    console.log('weixinLogin result', result.data);
                    // localStorage.setItem("loginLoading", '0');
                    if (result.data.code === 200) {
                        let data = result.data.data;
                        if (data&&data.sso) {
                            let ssoId = data.sso.id;
                            let oauthId = data.sso.oauthId;
                            let avatarUrl = data.sso.avatarUrl;
                            let nickname = data.sso.nickname;
                            //卓星账号
                            // let ssoId = 20111610;
                            // let oauthId = 'og_h10i1_l5muIu73VB-evPYY1Rk';
                            // let avatarUrl = 'http://thirdwx.qlogo.cn/mmopen/vi_32/uRSd4P8a94PhMJ5Lp4IydqrCnoS3iaMSYy53JSQQWIiaChEicTq0ibeLxHXqjsiaL6ZyDjiaYeHeFQTPDSwCejXAXFCA/132';
                            // let nickname = '飞  流 星';

                            //红涛账号
                            // let ssoId = 20111594;
                            // let oauthId = 'og_h10rR1Tc_7VMXMbxVj7lXGNdo';
                            // let avatarUrl = 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoLG6lTcYjVo7Zf1FVXf5ypj7fLK1BAAkwgTO5NzXnkmWGvSyicqVgg1k7ytCUCYUm6UzSlt9TeCQg/132';
                            // let nickname = '李红涛';
                            this.props.history.push(`/bind-phone?ssoId=${ssoId}&oauthId=${oauthId}&&avatarUrl=${avatarUrl}&name=${nickname}`);
                        }
                        else {
                            let loginToken =  data.loginToken;
                            // let loginToken = 'ae674f7ec1ce09b124ed21471e9780c4';
                            // let loginToken = '41f4fc0c7ec43f68f66bb3932901dded';
                            teacherInfor(loginToken).then((result) => {
                                console.log('teacherInfor this', this, '\n result', result);
                                if (result.data && result.data.code === 200) {
                                    localStorage.setItem(LOGINTOKEN, loginToken); //红涛
                                    localStorage.setItem(USERINFO, JSON.stringify(result.data.data));
                                    console.log('登录中');
                                    this.props.dispatch(saveLoginInfo(loginToken, result.data.data));
                                    console.log('登录中1');
                                    this.props.history.replace('/');
                                    console.log('登录中2');
                                }
                                else {
                                    message.warning(result.data.error);
                                }

                            }).catch((error) => {

                            })

                        }

                    }
                    else {
                        message.warning(result.data.error);
                    }

                }).catch((error) => {


                });
            }

        }

    }

    render() {

        const {showWeixin} = this.state;
        let url =  `https://open.weixin.qq.com/connect/qrconnect?appid=wx41ddc056b87016ba&redirect_uri=${weixinRedirectUri}&response_type=code&scope=snsapi_login#wechat_redirect`;
        return <div className='login'>
            {
                showWeixin ?
                    <iframe
                        className='login-iframe'
                        seamless='seamless'
                        frameBorder='0'
                        src={url}>
                    </iframe>
                    :
                    <div className='login-bg'>
                        <div className='login-title'>海码王教学系统</div>
                        <Button type="primary" className="weixin-login" onClick={this.login.bind(this)}>微信登录</Button>
                    </div>
            }
        </div>
    }

}

export default Login1;
