
/**
 * 完整课件
 * */
import React from 'react'
import './index.less'
import Header from "../../components/header";
import Content from "../../components/content";
import {leaveMessageFetch,homeworkMessageUpdateFetch} from "../../service/courseFetch";
import { message,Pagination,Spin} from 'antd';
import moment from "moment/moment";
import GeneralEmpty from "../../components/generalEmpty";
import {LOGINTOKEN} from "../../utils/constants";


export  default  class StudentLeaveMessage  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            isLoading:false,
            data:[],
            totalCount:0,
            solveCount:0,
            currentPage:0,
            sort:2,
        };

        console.log('studentLeaveMessage constructor ');

    }
    componentWillMount() {
        console.log('studentLeaveMessage componentWillMount ');
    }
    componentDidMount(){
        console.log('studentLeaveMessage componentDidMount ');
        this.listFetch();
    }
    componentWillReceiveProps(nextProps){
        console.log('studentLeaveMessage componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('studentLeaveMessage \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){
        let titles = localStorage.getItem('courseTitle');
        titles = JSON.parse(titles);
        let title1 = titles.title1;
        let title2 = titles.title2;
        let title3 = titles.title3;
        let title4 = titles.title4;
        let title5 = null;
        let title6 = null;

        let type = titles.type;
        if (type === 2)
        {
            title4 = '学生留言';
        }
        else if(type === 3)
        {
            title5 = '学生留言';
        }


        return <div>
            <Header type={type} hiddenCreation={true} title1={title1} title2 = {title2} title3={title3} title4={title4} title5={title5} title6={title6}/>
            <Content>
                <div className='message-root'>
                    <div className='message-body-root'>
                        <div className='header'>
                            <div className='first'>已解决({`${this.state.solveCount}/${this.state.totalCount}`})</div>
                            <div className='second'>留言人</div>
                            <div className='third'>留言内容</div>
                            <div className='four' onClick={this.sortClick.bind(this)}>留言时间
                                 <img className='sort' src={require('../../static/img/icon_sort.png')}/>
                            </div>
                        </div>
                        {this.showBody.bind(this)()}
                    </div>
                </div>
            </Content>
        </div>
    }

    showBody = ()=>{

        if (this.state.isLoading){

            return <Spin size="large" style={{"fontSize":"30px","display":'block','margin':'300px auto 0px'}}/>;
        }
        if (!this.state.data || this.state.data.length === 0)
        {
            return <GeneralEmpty/>
        }
        return    <div className='message-list'>
            {this.state.data&&this.state.data.map((item,index)=>{

                const {studentName =''} = item;
                let style = {
                    opacity:1
                }
                if (item.state === 2)
                {
                    style = {
                        opacity:0.55
                    }
                }
                let createdAt = moment(item.createdAt).format("YYYY-MM-DD");
                return  <div className='message-list-row' key={index}>
                    <div className='top'>
                        <img className='check' onClick={this.checkClick.bind(this,item)} src={item.state === 2?require('../../static/img/icon_select.png'):require('../../static/img/icon_unselect.png')}/>
                        <div className='student-infor' style={style}>
                            <img className='head-portrait' src={item.studentAvatarUrl}/>
                            <div className='name'>{studentName}</div>
                        </div>
                        <div className='message-content' style={style}>
                            <div className={item.showBottom?(item.on?'content-unfold':'content-fold'):'content-unfold'}>{item.content}</div>
                            {
                                item.showBottom? <div className='unfold' >
                                                      <span className='touch-content' onClick={this.onFoldClick.bind(this,item)}>
                                                          {item.on?'收起':'展开'}
                                                          <img className='img' src={item.on?require('../../static/img/icon_fold.png'):require('../../static/img/icon_unfold.png')}/>
                                                      </span>
                                </div>:''
                            }
                        </div>
                        <div className='time' style={style}>{createdAt}</div>
                    </div>
                    <div className='bottom'></div>
                </div>
            })}
            <div className='pagination'>
                <Pagination pageSize={10} total={this.state.totalCount}
                            current={this.state.currentPage+1}
                            hideOnSinglePage={true}
                            onChange={this.onChangePage.bind(this)}/>
            </div>
        </div>
    }

    listFetch = ()=>{

        let loginToken =  localStorage.getItem(LOGINTOKEN);
        let courseId = this.props.match.params.courseId;
        let teacherCourseId = this.props.match.params.teacherCourseId;
        let courseLessonId = this.props.match.params.courseLessonId;
        let sort = this.state.sort;
        let currentPage = this.state.currentPage;

        console.log('listFetch params ',this.props.match.params);
        this.setState({
            isLoading:true,
        })
        leaveMessageFetch(loginToken,courseId,teacherCourseId,courseLessonId,sort,currentPage).then((result)=>{

            console.log('result',result.data);
            if (result.data && result.data.code === 200)
            {

                let data = result.data.data.content;
                // for (let i=0;i<10;i++)
                // {
                //     let tempItem = data[0];
                //     var newObj = JSON.parse(JSON.stringify(tempItem));
                //     data.push(newObj);
                // }
                // for (let i = 0; i<data.length; i++)
                // {
                //     let value = data[i];
                //     if (i===1)
                //     {
                //         value.content = value.content  + '在CSS中，两个或以上的块元素（可能是兄弟，也可能不是），在CSS中，两个或以上的块元素（可能是兄弟，也可能不是）之间的相邻外边距可之间的相邻外边距可以被合并成一个单独的外边距。通过此方式合并的外边距被称为折叠，且产生的已合并的外边距被称为折叠外边距。关于什么时候用margin什么时候用padding，网上有许许多多的讨论，大多数似乎讨论到点上面，却又有些隔靴搔痒的感觉，总是答不到点上。而且margin和padding在许多地方往往效果都是一模一样，而且你也不能说这个一定得用margin那个一定要用padding，因为实际的效果都一样，你说margin用起来好他说padding用起来会更好，但往往争论无果。根据网上的总结归纳大致发现这几条还是比较靠谱的：';
                //
                //     }
                //     else if (i===2)
                //     {
                //         value.content = value.content  + '需要在border内测添加空白时。空白处需要背景（色）时。\n 上下相连的两个盒子之间的空白，希望等于两者之和时。如15px + 20px的padding，将得到35px的空白。上下相连的两个盒子之间的空白，希望等于两者之和时。如15px + 20px的padding，将得到35px的空白,上下相连的两个盒子之间的空白，希望等于两者之和时。如15px + 20px的padding，将得到35px的空白'
                //     }
                //     else if(i===3)
                //     {
                //         value.content = value.content  + '';
                //     }
                // }
                let  solveCount =0;
                for (let value  of result.data.data.extendParam)
                {
                    if (value.t === 'B')
                    {
                        solveCount = value.c;
                    }
                }
                this.setState({
                    data:data,
                    solveCount:solveCount,
                    totalCount:result.data.data.pageable.totalSize,
                    isLoading:false,
                },()=>{
                    // let rowArray =  document.getElementsByClassName('content-unfold');
                    //
                    // for (let i = 0; i <rowArray.length;i++)
                    // {
                    //     let  row = rowArray[i];
                    //     let clientHeight = row.clientHeight;
                    //     if (clientHeight>62)
                    //     {
                    //         let rowData =   this.state.data[i];
                    //         rowData.showBottom =  true;
                    //     }
                    //     console.log('clientHeight',row,row.offsetHeight,row.clientHeight);
                    // }
                    //
                    // this.setState({})
                })
            }
            else
            {
                message.warning(result.data.error);
                this.setState({
                    isLoading:false,
                })
            }

        }).catch(()=>{
            this.setState({
                isLoading:false,
            })
        })
    }
    sortClick = ()=>{
        console.log('点击顺序按钮');
        if (this.state.sort === 2)
        {
            this.state.sort = 1;
        }
        else {
            this.state.sort = 2;
        }
        this.listFetch();
    }
    checkClick = (item)=>{
       let solveCount = this.state.solveCount;
       let tempState = item.state;
       let tempCount = solveCount;

        if (item.state === 2)
        {
            item.state = 1;
            solveCount -= 1;
        }
        else {
            item.state = 2;
            solveCount += 1;
        }
        this.setState({
            solveCount:solveCount,
        });

        let loginToken =  localStorage.getItem(LOGINTOKEN);
        homeworkMessageUpdateFetch(loginToken,item.id,item.state).then((result)=>{

            if (result.data && result.data.code === 200)
            {
                console.log('更新成功');
            }
            else {
                message.warning(result.data.error);
                item.state = tempState;
                this.setState({
                    solveCount:tempCount,
                })
            }

        }).catch((error)=>{

        })

    }
    onFoldClick = (item)=>{
        item.on = !item.on;
        this.setState({});

    }
    onChangePage = (page, pageSize)=>{
        console.log('onChangePage',page,pageSize)
        this.state.currentPage = page-1;
        this.listFetch();
    }

}
