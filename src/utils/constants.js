
export const isTest =  window.location.href.includes('https://www.haimacode.com/test/HaimaTeacher/teacher') || window.location.href.includes('http://localhost') ;
export const apiBaseUrl = isTest ? 'https://www.haimacode.com/test/api/haimawang' : 'https://www.haimacode.com/api/haimawang';
export const apiServiceUrl = isTest ? 'https://www.haimacode.com/test/api/service' : 'https://www.haimacode.com/api/service';

//gui 地址
export const jumpApi = isTest ? 'https://www.haimacode.com/test/HaimaTeacher/scratch/index.html':'https://www.haimacode.com/t/scratch/index.html';
// export const jumpApi = 'http://192.168.0.107:8601';
//pdf 地址
export const  pdfUrl = isTest? 'https://www.haimacode.com/test/HaimaTeacher/pdf/generic/web/viewer.html?file=':'https://www.haimacode.com/t/pdf/generic/web/viewer.html?file=';

//微信回调地址 https://www.huazilive.com/test/HaimaTeacher/Teacher/index.html#/login
export const loginUri = isTest ? 'https://www.haimacode.com/test/HaimaTeacher/teacher/index.html#/login':'https://www.haimacode.com/t/teacher/index.html#/login';
export const weixinRedirectUri = encodeURIComponent(loginUri);
export const appId = '20111113';
export const appKey = 'ab731a584eb3826cf1sdf6ee654c2d32';
export const accountType = 'teacher';

//token
export const LOGINTOKEN = 'HaimaTeacherLoginToken';
//用户信息A
export const USERINFO = 'userInfo';

