
/**
 * 账号设置
 * */
import React from 'react'
import './index.less'
import {Button,message,Input,Upload,Icon} from 'antd';
import {updateTeacherInfor} from "../../service/personFetch";
import {connect} from "react-redux";
import {saveLoginInfo,update} from "../../redux/actions/loginactions";
import {apiBaseUrl, LOGINTOKEN, USERINFO} from "../../utils/constants";

import { bindActionCreators,compose } from 'redux';
import * as actions from "../../redux/actions/loginactions";
import * as saveActions from "../../redux/actions/saveActions";

@connect(
    ({login:{token,userinfo}}) => ({token,userinfo}),
    // dispatch =>bindActionCreators(actions, dispatch),
    // dispatch =>{
    //    return{
    //        actions:bindActionCreators(actions, dispatch),
    //        saveActions:bindActionCreators(saveActions, dispatch),
    //    };
    // }
)

class AccountSettings  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        const {avatarUrl,name}  = localStorage.getItem(USERINFO)&&JSON.parse(localStorage.getItem(USERINFO));
        let loginToken =  localStorage.getItem(LOGINTOKEN);
        this.state = {
            avatarUrl:avatarUrl,
            name:name,
            uploadData:{
                loginToken:loginToken
            },
        };
        console.log('AccountSettings constructor ');
    }
    componentWillMount() {
        console.log('AccountSettings componentWillMount ');
    }
    componentDidMount(){
        console.log('AccountSettings componentDidMount ');
    }
    componentWillReceiveProps(nextProps){
        console.log('AccountSettings componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('AccountSettings \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){

        const  {avatarUrl,name,loading} = this.state;
        return <div className='account-settings-root'>
            <div className='base-infor'>基本信息</div>
            <div className='head'>
                <div className='head-title'>头{'\xa0\xa0\xa0\xa0'}像:</div>
                <img className='head-img' src={avatarUrl}/>
                <Upload className='upload'
                        accept={'.jpg,.jpeg,.png,.gif,.bmp,.JPG,.JPEG,.PBG,.GIF,.BMP'}
                        action={apiBaseUrl+"/account/teacher/uploadFile"}
                        data={this.state.uploadData}
                        name={'upload'}
                        showUploadList={false}
                        onChange={this.uploadChange.bind(this)}
                        beforeUpload={this.beforeUpload}
                >
                    {/*<img className='edit' src={require('../../static/img/icon_edit_green.png')}/>*/}
                    <Icon style={{ color: '#54C3D4', cursor: 'pointer' }} type={loading ? 'loading' : 'edit'} />
                </Upload>
            </div>
            <div className='name'>
                <div className='name-title'>用户名:</div>
                <Input ref={(ref)=>{this.inputName = ref}} className='input' placeholder="请输入您的用户名" defaultValue={name}/>
            </div>
            <Button type="primary" className="save" onClick={this.saveInfo.bind(this)}>保存</Button>
        </div>
    }
    beforeUpload = (file) => {
        return new Promise((res, rej) => {
            if (file.size > 1024 * 1024 * 10) {
                message.info('上传图片大小不能超过10M')
                rej(file)
                return
            }
            res(file)
        })
    }
    uploadChange = (info)=>{
        console.log('头像',info)
        console.log('头像 info.file.status',info.file.status);
        console.log('头像 info.file.response',info.file.response);
        if (info.file.status === 'uploading') {
            console.log('上传中');
            this.setState({ loading: true });
        }
        else if (info.file.status === 'done')
        {
            console.log('上传结束');
            if (info.file.response.code === 200) {
                message.success('上传成功');
                this.setState({
                    avatarUrl:info.file.response.data.url,
                    loading: false,
                })
            } else{
                message.error('上传失败');
                this.setState({ loading: false });
            }
        }
        else {
            console.log('上传未知');
            this.setState({ loading: false });
        }

    }
    saveInfo = ()=>{
        console.log('this. input' ,this.inputName.state.value);
        let loginToken =  localStorage.getItem(LOGINTOKEN);
        console.log('this.props.dispatch',this.props.dispatch);
        updateTeacherInfor(loginToken, this.inputName.state.value, this.state.avatarUrl)
            .then((result) => {
                if (result.data && result.data.code === 200){
                    localStorage.setItem(USERINFO,JSON.stringify(result.data.data));
                    console.log('this.props',this.props);
                    this.props.dispatch(saveLoginInfo(loginToken, result.data.data));
                    console.log('this.props2',this.props);
                    message.success('更新成功');
                    // this.props.dispatch.update(loginToken, result.data.data);
                }
                else
                {
                    message.error('更新失败');
                }
            }).catch((error) => {

            });
    }
}

export default AccountSettings;
