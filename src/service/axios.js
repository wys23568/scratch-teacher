import axios from 'axios';
import Qs from 'qs';
import {apiBaseUrl, apiServiceUrl,appId,appKey} from "../utils/constants";


const Axios = axios.create({
    baseURL: apiBaseUrl,
    timeout: 10000,
    headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
        // 对 data 进行任意转换处理
        return Qs.stringify(data);
    }],
    transformResponse: [function (data) {
        // 对 data 进行任意转换处理
        return JSON.parse(data);
    }]
});

const AxiosApiService = axios.create({
    baseURL: apiServiceUrl,
    timeout: 10000,
    headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformRequest: [function (data) {
        // 对 data 进行任意转换处理
        return Qs.stringify(data);
    }],
    transformResponse: [function (data) {
        // 对 data 进行任意转换处理
        return JSON.parse(data);
    }]
});

export const AxiosPost = (url,data)=>{
    data.appId = appId;
    data.appKey = appKey;
    return Axios.post(url, data);
};
export const AxiosApiServicePost = (url,data)=>{
    data.appId = appId;
    data.appKey = appKey;
    return AxiosApiService.post(url, data);
};

export const AxiosGet = (url,data)=>{
   let tempData = {
        params: {
            ...data,
            appId: appId,
            appKey: appKey
        }
    }
    return Axios.get(url, tempData);
};
