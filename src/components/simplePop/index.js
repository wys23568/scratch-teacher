
/**
 * 简单通用弹框
 * */
import React from 'react'
import './index.less'

import { Button} from 'antd';

export  default  class SimplePop  extends React.Component {

    static defaultProps={
        title:'提醒',
        content:'内容',
        cancelTitle:'取消',
        okTitle:'确认'
    };
    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {

        };
        console.log('SimplePop constructor');
    }
    componentWillMount() {
        console.log('SimplePop componentWillMount ');
    }
    componentDidMount(){
        console.log('SimplePop componentDidMount ');
    }
    componentWillReceiveProps(nextProps){
        console.log('SimplePop componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('SimplePop \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){
        return <div className='simple-root'>
            <div className='simple-title'>{this.props.title}</div>
            <div className='simple-content'>{this.props.content}</div>
            <div className='simple-actions'>
                <Button  className="cancel" onClick={this.onCancelPop.bind(this)}>{this.props.cancelTitle}</Button>
                <Button type="primary" className="complete" onClick={this.onOkPop.bind(this)}>{this.props.okTitle}</Button>
            </div>
        </div>
    }
    onCancelPop =()=>{
        this.props.onCancelPop();
    }
    onOkPop =()=>{
        this.props.onOkPop();
    }

}
