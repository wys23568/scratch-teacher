
/**
 * 查看更多
 * */
import React from 'react'
import './index.less'
import Header from "../../components/header";
import Content from "../../components/content";
import {courseListFetch} from "../../service/courseFetch";
import CourseListView from "../../components/courseListView";
import {LOGINTOKEN} from "../../utils/constants";

import { message,Pagination,Spin} from 'antd';
import GeneralEmpty from "../../components/generalEmpty";

export  default  class CheckMoreCourse  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            isLoading:false,
            data:[],
            currentPage:0,
        };

        console.log('CheckMoreCourse constructor ');

    }
    componentWillMount() {
        console.log('CheckMoreCourse componentWillMount ');
    }
    componentDidMount(){
        console.log('CheckMoreCourse componentDidMount ');
        this.listFetch();
    }
    componentWillReceiveProps(nextProps){
        console.log('CheckMoreCourse componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('CheckMoreCourse \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){
        let type = this.props.match.params.type;
        let title = '';
        if (type === '1')
        {
            title = '待开始课程';
        }
        else if (type === '2')
        {
            title = '进行中课程';
        }
        else if (type === '3')
        {
            title = '已完结课程';
        }

        return <div>
            <Header/>
            <Content>
                <div className='morecourse-root'>
                    <div className='header' onClick={this.goback.bind(this)}>
                        <img className='back-img' src={require('../../static/img/bar-back.png')}/>
                        <span>{title}</span>
                    </div>
                    {this.showBody()}
                </div>
            </Content>
        </div>
    }
    showBody = ()=>{
        if (this.state.isLoading)
        {
            return <Spin size="large" style={{"fontSize": "30px", "display": 'block', 'margin': '320px auto'}}/>;
        }
        if (!this.state.data.length){

            return  <GeneralEmpty hintText={'暂无课程'}/>
        }
        return <div className='list-bg'>
            <CourseListView courseList={this.courseList.bind(this)} data={this.state.data}/>
            <div className='pagination'>
                {
                    this.state.totalSize > 12 ? <Pagination pageSize={12} total={this.state.totalSize}
                                                            className='pagination-p'
                                                            current={this.state.currentPage + 1}
                                                            hideOnSinglePage={true}
                                                            onChange={this.onChangePage.bind(this)}/> : ''
                }
            </div>
        </div>
    }
    listFetch = ()=>{
        let loginToken =  localStorage.getItem(LOGINTOKEN);
        if (loginToken&& loginToken.length)
        {
            this.setState({
                isLoading:true,
            })
            let type = this.props.match.params.type;
            courseListFetch(loginToken,type,null,this.state.currentPage,12).then((result)=>{
                console.log('courseListFetch this',this,'\n result',result);
                if (result.data && result.data.code === 200)
                {
                    let data = result.data.data.content;
                    // for (let i=0;i<10;i++)
                    // {
                    //     let tempItem = result.data.data.content[0];
                    //     var newObj = JSON.parse(JSON.stringify(tempItem));
                    //     data.push(newObj);
                    // }
                    this.setState({
                        isLoading:false,
                        data:data,
                        totalSize:result.data.data.pageable.totalSize ,
                    })
                }
                else
                {
                    message.warning(result.data.error);
                    this.setState({
                        isLoading:false,
                    })
                }

            }).catch((error)=>{
                this.setState({
                    isLoading:false,
                })
                console.log('error',error);
            })
        }
    }

    courseList = (item)=>{
        console.log('courseList',item);
        var titles = {title1:item.name,title2:item.issue+'期'};
        titles = JSON.stringify(titles);
        localStorage.setItem("courseTitle",titles);
        var path = `/course-lesson-list/${item.id}/${item.teacherCourseId}`;
        this.props.history.push(path);

    }
    goback = ()=>{
        this.props.history.goBack();
    }
    onChangePage = (page, pageSize)=>{
        console.log('onChangePage',page,pageSize)
        this.state.currentPage = page-1;
        this.listFetch();
    }

}
