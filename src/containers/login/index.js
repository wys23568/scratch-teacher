import React from 'react'
import './index.less'

import {Button, message} from 'antd';
import {saveLoginInfo} from "../../redux/actions/loginactions";
import {connect} from "react-redux";
import {GetSearchQueryString, GetHrefQueryString} from "../../utils/commenutil";
import {teacherInfor, weixinLogin} from "../../service/personFetch";
import {
    LOGINTOKEN,
    USERINFO,
    weixinRedirectUri,
    accountType,
    loginUri
} from "../../utils/constants";

@connect()
class Login extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {

        };
        console.log('Login constructor', this.props);
        localStorage.setItem(LOGINTOKEN, '');
        localStorage.setItem(USERINFO, '');
        let loginToken = localStorage.getItem(LOGINTOKEN);
        let userInfo = localStorage.getItem(USERINFO);;
        this.props.dispatch(saveLoginInfo(loginToken, userInfo));
    }

    componentWillMount() {
        console.log('Login componentWillMount ');
    }

    componentDidMount() {
        console.log('Login componentDidMount ');
        console.log('loginUri =',loginUri);
        console.log('weixinRedirectUri =',weixinRedirectUri);
    }

    componentWillReceiveProps(nextProps) {
        console.log('Login componentWillReceiveProps', nextProps);
    }

    componentDidUpdate(preProps) {
        console.log('Login componentDidUpdate', preProps);
        let code = GetHrefQueryString('code');
        console.log('code:', code);
        if(code)
        {
            window.history.replaceState(null,null,loginUri);
            this.getWeixinCode.bind(this,code)();
        }
    }

    getWeixinCode = (code) => {
        if (code != null) {
            weixinLogin(accountType, code).then((result) => {
                console.log('weixinLogin result', result.data);
                if (result.data.code === 200) {
                    let data = result.data.data;
                    if (data && data.sso) {
                        let ssoId = data.sso.id;
                        let oauthId = data.sso.oauthId;
                        let avatarUrl = data.sso.avatarUrl;
                        let nickname = data.sso.nickname;
                        this.props.history.push(`/bind-phone?ssoId=${ssoId}&oauthId=${oauthId}&&avatarUrl=${avatarUrl}&name=${nickname}`);
                    }
                    else {
                        let loginToken = data.loginToken;
                        teacherInfor(loginToken).then((result) => {
                            console.log('teacherInfor this', this, '\n result', result);
                            if (result.data && result.data.code === 200) {
                                localStorage.setItem(LOGINTOKEN, loginToken); //红涛
                                localStorage.setItem(USERINFO, JSON.stringify(result.data.data));
                                console.log('登录中');
                                this.props.dispatch(saveLoginInfo(loginToken, result.data.data));
                                console.log('登录中1');
                                this.props.history.replace('/');
                                console.log('登录中2');
                            }
                            else {
                                message.warning(result.data.error);
                            }

                        }).catch((error) => {

                        })

                    }

                }
                else {
                    message.warning(result.data.error);
                }

            }).catch((error) => {

            });
        }
    }

    render() {

        let url =  `https://open.weixin.qq.com/connect/qrconnect?appid=wx2b8b6b3771f9a10e&redirect_uri=${weixinRedirectUri}&response_type=code&scope=snsapi_login#wechat_redirect`;
        return <div className='login'>
            <div className='login-center'>
                <iframe
                    className='login-iframe'
                    seamless='seamless'
                    frameBorder='0'
                    src={url}>
                </iframe>
                <div className='login-title'>海码王教学系统</div>
            </div>
        </div>
    }

}

export default Login;
