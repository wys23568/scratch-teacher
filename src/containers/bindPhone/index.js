
/**
 * 绑定手机号
 * */
import React from 'react'
import './index.less'
import { Input,Button,Checkbox,message,Form} from 'antd';
import {saveLoginInfo} from "../../redux/actions/loginactions";
import {LOGINTOKEN, USERINFO} from "../../utils/constants";

import {
    withRouter
} from "react-router-dom";
import {connect} from "react-redux";
import {teacherInfor, thirdPartyBindphone, thirdPartyGetCode} from "../../service/personFetch";

const FormItem = Form.Item;

@connect()
 class BindPhone  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            checked:true,
            verificationBtnDisabled:false,
            time: 60,
        };

        console.log('BindPhone constructor',this.props);
    }
    componentWillMount() {
        console.log('bindPhone componentWillMount ');
    }
    componentDidMount(){
        console.log('bindPhone componentDidMount ');
    }
    componentWillReceiveProps(nextProps){
        console.log('bindPhone componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('bindPhone \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    componentWillUnmount() {
        clearTimeout(this.timer);
    }
    render(){
        const { getFieldDecorator } = this.props.form;

        return <div className='bind-root'>
            <div className='bind-bg'>
                <div className='header'>
                    <img className='logo-img' src={require('../../static/img/logo.png')}/>
                </div>
                <div className='body'>
                    <div className='bing-title'>请输入用于账号验证的手机号</div>
                    <Form>
                        <FormItem>
                            {getFieldDecorator('phone', {
                                rules: [{ required: true, message: '手机号不能为空!' },{validator:this.accountSetnewPhoneValidation.bind(this)}],
                            })(
                                <Input className='input-number'
                                maxLength={11} allowClear
                                placeholder="请输入手机号"  onChange={this.inputChange.bind(this)}/>
                            )}
                        </FormItem>
                    </Form>
                    <div className='verificvation-code-bg'>
                        <Input className='input-code' allowClear placeholder="请输入验证码" value={this.state.codeInput} onChange={this.codeInput.bind(this)}/>
                        <Button type="primary"
                                className="verificvation-code"
                                onClick={this.verificationCode.bind(this)}
                                disabled={this.state.verificationBtnDisabled}
                        >
                            {!this.state.verificationBtnDisabled ? '获取验证码' : `${this.state.time}s`}
                        </Button>
                    </div>
                    {/*<div className='user-agreement-bg'>*/}
                        {/*<Checkbox checked={this.state.checked} onChange={(e)=>{*/}
                            {/*console.log('Checkbox ',e.target.checked);*/}
                            {/*this.setState({*/}
                                {/*checked:e.target.checked,*/}
                            {/*})*/}
                        {/*}}/>*/}
                        {/*<span>我已阅读并同意 <span className='agreement' onClick={this.userAgreement}>《用户协议》</span></span>*/}
                    {/*</div>*/}
                    <div className='bind-phone-bg'>
                        <Button type="primary" disabled={!this.state.checked} className="bind-phone" onClick={this.bindPhone.bind(this)}>登录</Button>
                    </div>
                </div>
            </div>
        </div>
    }
    //新手机号输入
    accountSetnewPhoneValidation(rule, value, callback){
        console.log('账号'+value)
        let re = /^1\d{10}$/;//账号正则验证
        if(!re.test(value)&&value!=''&&typeof(value)!='undefined'){
            callback('账号格式不对！')
        }else{
            callback()
        }
    }
    inputChange = (e)=>{
        console.log('inputChange',e.target.value);
        this.setState({
            phoneInput:e.target.value,
        })
    }
    codeInput = (e)=>{
        console.log('codeInput',e.target.value);
        this.setState({
            codeInput:e.target.value,
        })
    }
    verificationCode= (e)=>{
        console.log('获取验证码',this.state.phoneInput);
        if (!this.state.phoneInput || this.state.phoneInput.length !== 11)
        {
            message.warning('请输入正确手机号');
            return;
        }
        this.setState({verificationBtnDisabled: true});
        this.timer = setInterval(() => {
            const { time } = this.state;
            this.setState({ time: time - 1, verificationBtnDisabled: true },
                () => {
                    if (time === 1) {
                        clearInterval(this.timer);
                        this.setState({
                            time: 60,
                            verificationBtnDisabled: false
                        })
                    }
                })
        }, 1000)
        let params = new URLSearchParams(this.props.location.search);
        let ssoId = params.get('ssoId');
        let oauthId = params.get('oauthId');
        thirdPartyGetCode(ssoId,oauthId,this.state.phoneInput).then((result)=>{

            console.log('thirdPartyGetCode',result.data);
            if (result.data && result.data.code === 200)
            {
                message.success('验证码已发送');
            }
            else {
                message.warning(result.data.error);
                clearTimeout(this.timer);
                this.setState({
                    time: 60,
                    verificationBtnDisabled: false
                })
            }
        }).catch((error)=>{

        })

    }
    userAgreement = (e)=>{
        console.log('用户协议',e);

    }
    bindPhone = ()=>{
        console.log('绑定手机');
        if (!this.state.phoneInput || this.state.phoneInput.length !== 11)
        {
            message.warning('请输入正确手机号');
            return;
        }
        if (!this.state.codeInput)
        {
            message.warning('请输入验证码');
            return;
        }

        let params = new URLSearchParams(this.props.location.search);
        let ssoId = params.get('ssoId');
        let oauthId = params.get('oauthId');
        thirdPartyBindphone(ssoId,oauthId,this.state.phoneInput,this.state.codeInput).then((result)=>{

            console.log('thirdPartyBindphone',result.data);
            if (result.data.code === 200)
            {
                let loginToken = result.data.data.loginToken;
                // let loginToken = 'ae674f7ec1ce09b124ed21471e9780c4';
                let  useFlag = 2;
                let  name = params.get('name');
                let  avatarUrl = params.get('avatarUrl');
                teacherInfor(loginToken,useFlag,name,avatarUrl).then((result)=>{
                    console.log('teacherInfor this',this,'\n result',result);
                    if (result.data && result.data.code === 200)
                    {
                        localStorage.setItem(LOGINTOKEN, loginToken); //红涛
                        localStorage.setItem(USERINFO,JSON.stringify(result.data.data));
                        this.props.dispatch(saveLoginInfo(loginToken, result.data.data));
                        this.props.history.replace('/');
                    }
                    else {
                        if (result.data.code === 100)
                        {
                            this.props.history.goBack();
                            message.warning(result.data.error);
                        }
                        else {
                            message.warning(result.data.error);
                        }
                    }

                }).catch((error)=>{
                })
            }
            else {
                message.warning(result.data.error);
            }
        }).catch((error)=>{

        })

    }

}
const WrappedRegistrationForm = Form.create()(BindPhone);
export default withRouter(WrappedRegistrationForm);
