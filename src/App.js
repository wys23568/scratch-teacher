import React, {Component} from 'react';
import {
    HashRouter as Router,
    Route,
    Redirect,
    Switch
} from "react-router-dom";

import './app.less'
import { Layout,Spin } from 'antd';

import Login from "./containers/login";
import BindPhone from "./containers/bindPhone";
// import Layouts from "./containers/layouts";
import { connect } from 'react-redux'
import {saveLoginInfo} from "./redux/actions/loginactions";
import {teacherInfor} from "./service/personFetch";

import {isTest, LOGINTOKEN, USERINFO} from "./utils/constants";

const CourseManagement = React.lazy(() => import('./containers/courseManagement'));
const FullCourFseware = React.lazy(() => import('./containers/fullCourseware'));
const StudentLeaveMessage = React.lazy(() => import('./containers/studentLeaveMessage'));
const PersonalCenter = React.lazy(() => import('./containers/personalCenter'));
const ClassRoom = React.lazy(() => import('./containers/classroom'));
const CheckMoreCourse = React.lazy(() => import('./containers/checkMoreCourse'));
const StudentWork = React.lazy(() => import('./containers/studentWork'));
const CourseLessonList = React.lazy(() => import('./containers/courseLessonList'));
const Publishworks = React.lazy(() => import('./containers/publishworks'));
const WorksDetail = React.lazy(() => import('./containers/worksDetail'));


const PrivateRoute = ({component: Component, isAuthenticated, ...rest}) => {
    return (
        <Route
            {...rest}
            render={props =>
                isAuthenticated ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: {from: props.location}
                        }}
                    />
                )
            }
        />
    );
}
@connect(({ login }) => ({ login }))
class App extends Component {
    constructor(props) {
        super(props);
        this.token =   localStorage.getItem(LOGINTOKEN);
        let userInfo = localStorage.getItem(USERINFO)?JSON.parse(localStorage.getItem(USERINFO)):'';
        this.token && this.props.dispatch(saveLoginInfo(this.token,userInfo))
        this.state = {
            isAuthenticated: this.token&&this.token.length? true : false
        }
        console.log('App constructor', this.state.isAuthenticated,this.props,this.token,'isTest =',isTest);
    }
    componentWillMount() {
        console.log('App componentWillMount ');
    }
    componentDidMount(){
        console.log('App componentDidMount ');
        let loginToken =  localStorage.getItem(LOGINTOKEN);
        if (loginToken&& loginToken.length)
        {
            teacherInfor(loginToken).then((result)=>{
                console.log('App teacherInfor this',this,'\n result',result);
                if (result.data && result.data.code === 200)
                {
                    localStorage.setItem(USERINFO,JSON.stringify(result.data.data));
                    this.props.dispatch(saveLoginInfo(loginToken, result.data.data));
                }

            }).catch((error)=>{

            })
        }

    }
    componentWillReceiveProps(nextProps){
        console.log('App componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot)
    {
        console.log('App componentDidUpdate \n prevProps', prevProps,'\n prevState',prevState,'\n snapshot',snapshot,'\n this.props',this.props,'\n this.state',this.state);
        console.log('App componentDidUpdate2 \n prevProps.login.token', prevProps.login.token,'\n this.props.login.token',this.props.login.token,'\n this.state.isAuthenticated',this.state.isAuthenticated);

        if (!prevProps.login.token && this.props.login.token && !this.state.isAuthenticated) {
            this.authenticate();
        } else if (prevProps.login.token && !this.props.login.token && this.state.isAuthenticated) {
            this.signout();
        }
    }
    authenticate = () => {
        console.log('app login',this.props);
        this.setState({ isAuthenticated: true });
    }
    signout = () => {
        console.log('app logout',this.props);
        this.setState({ isAuthenticated: false });
    }

    render() {
        const {isAuthenticated} = this.state;
        console.log('App render isAuthenticated',isAuthenticated);
        return (
            <Router>
                <React.Suspense fallback={<div className='user-lazy-div'><Spin size='large' tip='疯狂加载中...' /></div>}>
                <Layout className="layout">
                    {/*<Switch>*/}
                    {/*<Route path="/login" component={Login}/>*/}
                    {/*<Route path="/bind-phone/:ssoId/:oauthId" component={BindPhone}/>*/}
                    {/*<PrivateRoute isAuthenticated={isAuthenticated} path="/" component={Layouts}/> 必须写到最下面*/}
                    {/*</Switch>*/}
                    {/*/顺序随意*/}
                    <Route path="/login" component={Login}/>
                    <Route path="/bind-phone" component={BindPhone}/>
                    <PrivateRoute isAuthenticated={isAuthenticated} exact path="/"  component={CourseManagement} />
                    <PrivateRoute isAuthenticated={isAuthenticated} path="/course-lesson-list/:courseId/:teacherCourseId" component={CourseLessonList} />
                    <PrivateRoute isAuthenticated={isAuthenticated} path="/full-courseware/" component={FullCourFseware} />
                    <PrivateRoute isAuthenticated={isAuthenticated} path="/student-work/:courseId/:teacherCourseId/:courseLessonId" component={StudentWork} />
                    <PrivateRoute isAuthenticated={isAuthenticated} path="/student-leavemessage/:courseId/:teacherCourseId/:courseLessonId" component={StudentLeaveMessage} />
                    <PrivateRoute isAuthenticated={isAuthenticated} path="/check-morecourse/:type" component={CheckMoreCourse} />
                    <PrivateRoute isAuthenticated={isAuthenticated} path="/class-room/:courseId/:teacherCourseId/:courseLessonId" component={ClassRoom} />
                    <PrivateRoute isAuthenticated={isAuthenticated} path="/personal-center" component={PersonalCenter} />
                    <PrivateRoute isAuthenticated={isAuthenticated} path="/publish-works" component={Publishworks} />
                    <PrivateRoute isAuthenticated={isAuthenticated} path="/works-detail/:id" component={WorksDetail} />
                </Layout>
                </React.Suspense>
            </Router>
        );
    }
}

export default App;
