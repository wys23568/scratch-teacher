
/**
 * 作品详情
 * */
import React from 'react'
import './index.less'

import Header from "../../components/header";
import Content from "../../components/content";
import { Row, Col, Icon, Popover, Card, Button, Modal, Rate } from 'antd';
import styled from 'styled-components';
import {jumpApi, LOGINTOKEN} from "../../utils/constants";

import moment from 'moment';
import {productionDetailFetch} from "../../service/courseFetch";
const Userlabel = styled.div`
   height:39px;
   padding:6px 20px;
   font-size:13px;
   color:#54C3D4 ;
   background:#EEF8FB ;
   border-radius:31px;
   margin:4px 6px;
   line-height:25px;
   display:flex;
   justify-content:start;
   align-items:center;
`
export  default  class WorksDetail  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            showCartoon:true,
        };

        console.log('WorksDetail constructor ');

    }
    componentWillMount() {
        console.log('WorksDetail componentWillMount ');
    }
    componentDidMount(){
        console.log('WorksDetail componentDidMount ');
        this.getWorksDetail();
    }
    componentWillReceiveProps(nextProps){
        console.log('WorksDetail componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('WorksDetail \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){

        const { worksDetail = {},showCartoon } = this.state;
        const { productionTagInfoList = [], likeFlag, likeCount=0, content } = worksDetail;
        // src={'http://192.168.0.111:8601/?type=1&url=https://huazi-api-test.oss-cn-beijing.aliyuncs.com/storage/4038/260820/XNUf90ZRxBCsqZf17OGGDQlct11fvks.sb3'}
        // src={'http://192.168.0.111:8601/?type=1&url=https://huazi-api-test.oss-cn-beijing.aliyuncs.com/storage/4038/266826/XNkPXEZRxBCsqZf8nqTOkkNthggwR58V.sb3'}
        return <div>
             <Header/>
            <Content>
                <div className='hole-card'>
                    <Row className='works-detail'>
                        <Row type='flex' justify='start' className='parent'> <div className='stable' >
                            <Row className='works-detail-row1'>{worksDetail.name}</Row>
                            <Row type='flex' justify='start' align='middle' className='works-detail-row2'>
                                <Col><img style={{ width: '26px', height: '26px' ,borderRadius:'26px' }} src={worksDetail.authorAvatarUrl} alt='' /></Col>
                                <Col style={{ marginLeft: '8px' }}>{worksDetail.authorName}</Col>
                                <Col style={{ color: '#999999', marginLeft: '14px' }}>{`更新时间：\xa0${moment(worksDetail.updatedAt).format("YYYY-MM-DD")}`}</Col>
                            </Row>
                            {
                                content&& <iframe
                                    // style={{ display: showCartoon? 'none':'block' }}
                                    // onLoad={() => { this.setState({ showCartoon: false }) }}
                                    key={content}
                                    className='works-detail-row3'
                                    title='works-detail'
                                    seamless='seamless'
                                    frameBorder='0'
                                    src={`${jumpApi}?type=1&url=${content}`}
                                >
                                </iframe>
                            }
                            {
                                // showCartoon&&<div className='works-offline' style={{background:'#fff'}}><img
                                //     src={require('../../static/img/cartoon.gif')}
                                //     alt='' /></div>
                            }
                        </div><div className='change' >
                            <Row>
                                <Row type='flex' justify='end' style={{
                                    visibility:'hidden'
                                }}>
                                <Col className='works-star'> <Col style={{ fontSize: '36px', paddingRight: '10px', color: '#FFDC17'}}>
                                    {worksDetail.score ? worksDetail.score : '0.0'}</Col>
                                    <Rate style={{ fontSize: '16px' }} disabled allowHalf defaultValue={worksDetail.score ? worksDetail.score / 2 : 0} />
                                </Col>
                                </Row>
                                <Row className='col-row1'>作品介绍：</Row>
                                <Row className='col-row2'>{worksDetail.intro}</Row>
                                <Row className='col-row1'>操作介绍</Row>
                                <Row className='col-row2'>{worksDetail.operateExplain}</Row>
                                <Row className='col-row3'>{
                                    productionTagInfoList && productionTagInfoList.map((item) => {
                                        return <div key={item.id}><Userlabel>
                                            <img style={{ width: '18px', height: '14px', marginRight: '6px' }} src={item.imgUrl} alt='' />
                                            {item.name}
                                        </Userlabel>
                                        </div>
                                    })}</Row>
                            </Row>
                            <Row type='flex' justify='start' align='middle' className='col-row4'>
                                <Button
                                    onClick={this.adaptedWorks.bind(this, worksDetail.id)}
                                    style={{ width: '328px', height: '52px', borderRadius: '52px' }}
                                    type='primary'>编辑作品</Button></Row>
                        </div>
                        </Row>
                        <Row type='flex' justify='start' align='middle' className='works-detail-row4'>
                            <Col className='user-margin'>
                                <Icon style={{ fontSize: '20px', color: '#CDCFD7', marginRight: '6px' }} type="eye" theme="filled" />
                                {worksDetail.lookCount}
                            </Col>
                            <Col className='user-margin'>
                                <Icon
                                    // onClick={this.doLike.bind(this, worksDetail.id)}
                                    style={{ fontSize: '20px', color: likeFlag === 2 ? '#FFBF00' : '#CDCFD7', marginRight: '6px' }} type="like" theme="filled" />
                                {likeCount}
                            </Col>
                        </Row>
                    </Row>
                </div>
            </Content>
        </div>
    }

    getWorksDetail = ()=>{
        let loginToken =  localStorage.getItem(LOGINTOKEN);
        const { dispatch, match } = this.props;
        let productionId = match.params.id;
        productionDetailFetch(loginToken,productionId,2).then((result)=>{
            console.log('result',result.data);
            if (result.data && result.data.code === 200)
            {
                this.setState({
                    worksDetail:result.data.data,
                })
            }
            else
            {

            }
        }).catch((error)=>{


        });
    }
    adaptedWorks = (id) => {
        console.log(id)
        window.location.href = `${jumpApi}?type=3&productionId=${id}`
    }
}
