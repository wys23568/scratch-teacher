
/**
 * 发布作品
 * */
import React from 'react'
import './index.less'
import Header from "../../components/header";
import Content from "../../components/content";
import { Card, Row, Col, Button, Input, Icon, Checkbox, Upload, message } from 'antd';

import styled from 'styled-components';
import {getProductionTagFetch,getProductionCoverFetch,postPublishInfoFetch,productionDetailFetch} from "../../service/courseFetch";
import {apiBaseUrl, jumpApi, LOGINTOKEN} from "../../utils/constants";


const { TextArea } = Input;
const Usertag = styled.div`
   position: relative;
   height:28px;
   padding:4px 14px;
   font-size:14px;
   color:#54C3D4 ;
   background:#EEF8FB ;
   border-radius:28px;
   margin:0 9px 18px 9px;
   display:flex;
   justify-content:start;
   align-items:center;
   border:1px solid #54C3D4;
   cursor: pointer;
   -moz-user-select: none;
   -khtml-user-select: none;
   user-select: none;
`
const Userimg = styled.img`
width: 150px;
height: 120px;
margin: 5px 5px;
cursor: pointer;
border-radius: 5px;
transition:all 0.3s;
box-shadow:${props => props.hasChoose && '0px 5px 5px rgba(34, 25, 25, 0.5)'};
:hover{
    box-shadow:0px 5px 5px rgba(34, 25, 25, 0.5);
}
`
export  default  class Publishworks  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {
            tags: {},
            coverUrl: '',
            loading: false,
            title: '',
            introduce: '',
            explain: '',
            openFlag: '1'
        };
        let params = new URLSearchParams(this.props.location.search);
        console.log('Publishworks constructor ',params);

    }
    componentWillMount() {
        console.log('Publishworks componentWillMount ');
    }

    componentDidMount() {
        console.log('Publishworks componentDidMount ');
        let params = new URLSearchParams(this.props.location.search);
        this.setState({title: params.get('name'), coverUrl: params.get('coverUrl')})

        this.getProductionTags();
        this.getProductionCover();
        this.getProductionDetail();
    }
    componentWillReceiveProps(nextProps){
        console.log('Publishworks componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('Publishworks \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){
        let token =  localStorage.getItem(LOGINTOKEN);
        const { coverUrl, loading, title ,productionCover=[], productionTag=[],data={},imgId} = this.state;
        const {intro,operateExplain} =  data;
        console.log('render data',data,intro);
        return <div>
            <Header/>
            <Content>
                <div className='publish-works'>
                    <Card>
                        <div className='publish-parent'>
                            <div className='publish-change'>
                                <div className='publish-title'>发布作品</div>
                                <div className='publish-works-row'>
                                    <img className='red-tip' src={require('../../static/img/ic-red-tip.png')} />
                                    <div className='publish-works-left'>作品名称：</div>
                                    <Input
                                        value={title}
                                        maxLength={24}
                                        onChange={this.onChangeTitle}
                                        placeholder='请输入作品名称'
                                        style={{ width: '380px' }}>
                                    </Input>
                                </div>
                                <div className='publish-works-row'>
                                    <img className='red-tip' src={require('../../static/img/ic-red-tip.png')} />
                                    <div className='publish-works-left'>作品介绍：</div>
                                    <TextArea
                                        key={intro}
                                        onChange={this.onChangeIntroduce}
                                        maxLength={150}
                                        placeholder='介绍一下你的作品吧，让大家知道你的初衷是什么，分享你的想法～'
                                        // autosize={{ minRows: 6  }}
                                        rows = {6}
                                        defaultValue={intro}
                                        className='publish-works-right'>
                            </TextArea>
                                </div>
                                <div className='publish-works-row'>
                                    <img className='red-tip' src={require('../../static/img/ic-red-tip.png')} />
                                    <div className='publish-works-left'>操作说明：</div>
                                    <TextArea
                                        key={operateExplain}
                                        onChange={this.onChangeExplain}
                                        maxLength={150}
                                        placeholder='如果你的作品需要进一步的操作，请不要忘记告诉小伙伴们你的技巧哦～'
                                        // autosize={{ minRows: 6 }}
                                        rows = {6}
                                        defaultValue={operateExplain}
                                        className='publish-works-right'>
                            </TextArea>
                                </div>
                                <div className='publish-works-row'>
                                    <div className='publish-works-left'>作品标签：</div>
                                    <div className='publish-works-tags'>
                                        {
                                            productionTag.map(item => {
                                                return <Usertag key={item.id} onClick={this.onClickUsertag.bind(this, item.id)}>
                                                    <img style={{ width: '18px', height: '14px', marginRight: '4px' }} src={item.imgUrl} alt='' />
                                                    {item.name}
                                                    {this.state.tags[item.id] && <Icon className='position-icon' type="check-circle" theme="filled" />}
                                                </Usertag>
                                            })
                                        }
                                    </div>
                                </div>
                                <div className='publish-code'>
                                    <Checkbox
                                        defaultChecked={true}
                                        onChange={this.onchangeCheck}>
                                        开放源代码
                                    </Checkbox>
                                </div>
                            </div>
                            <div className='publish-stable'>
                                <img src={coverUrl} style={{ width: '480px', height: '300px', borderRadius: '5px' }} alt='' />
                                <Upload
                                    accept='image/*'
                                    action={`${apiBaseUrl}/account/teacher/uploadFile`}
                                    data={(file) => ({ loginToken: token, upload: file })}
                                    showUploadList={false}
                                    onChange={this.onChangeUpload}
                                    beforeUpload={this.beforeUpload}
                                >
                                    <div className='publish-stable-upload'>
                                        <Icon type={loading ? 'loading' : 'plus'} />
                                        <div style={{ paddingLeft: '6px' }}>上传照片</div>
                                    </div>
                                </Upload>
                                <div className='publish-cover'>
                                    {
                                        productionCover.map(item => {
                                            return <Userimg
                                                hasChoose={imgId === item.id}
                                                onClick={this.onClickUserimg.bind(this, item.imgUrl, item.id)}
                                                key={item.id} src={item.imgUrl}
                                                alt='' />
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                        <Row type='flex'  align='middle' style={{marginBottom:'10px'}}>
                            <Col onClick={this.onClickBack} className='row-col1'>
                                <img
                                    style={{ width: '22px', height: '18px', marginRight: '6px' }}
                                    src={require('../../static/img/icon-goback.png')} alt='' />
                                返回创作页</Col>
                            <Button
                                onClick={this.onClickPublish}
                                className='row-col2'
                                type='primary'>发布</Button>
                        </Row>
                    </Card>
                </div>
            </Content>
        </div>
    }

    getProductionTags = ()=>{

        getProductionTagFetch().then((result)=>{

            console.log('result',result.data);
            if (result.data && result.data.code === 200)
            {
                console.log('result data',result.data.data);
                this.setState({
                    productionTag:result.data.data,
                })
            }
            else {

            }
        }).catch(

        );
    }
    getProductionCover = ()=>{

        getProductionCoverFetch().then((result)=>{

            console.log('result',result.data);
            if (result.data && result.data.code === 200)
            {
                console.log('result data',result.data.data);
                this.setState({
                    productionCover:result.data.data,
                })
            }
            else {

            }
        }).catch(

        );
    }
    getProductionDetail = ()=>{
        let loginToken =  localStorage.getItem(LOGINTOKEN);
        let params = new URLSearchParams(this.props.location.search);
        let productionId = params.get('productionId')
        productionDetailFetch(loginToken,productionId).then((result)=>{
            console.log('result',result);
            if (result.data.code === 200)
            {
                let data = result.data.data;
                let tagIds = data.tagIds.split(',');
                let a = {};
                tagIds.forEach((item)=>{
                    a[item] = true;
                });
                // console.log('tagIds',tagIds,data.tagIds);
                this.setState({
                    data:data,
                    title:data.name,
                    introduce: data.intro,
                    explain: data.operateExplain,
                    tags:a
                });

            }
            else
            {

            }
        }).catch((error)=>{

        })
    }
    onChangeTitle = (e) => {
        this.setState({ title: e.target.value })
    }
    onChangeIntroduce = (e) => {
        this.setState({ introduce: e.target.value })
    }
    onChangeExplain = (e) => {
        this.setState({ explain: e.target.value })
    }
    onchangeCheck = (e) => {
        if (e.target.value) {
            this.setState({ openFlag: '1' })
            return
        }
        this.setState({ openFlag: '2' })
    }
    onClickUsertag = (id) => {
        this.setState(preState => {
            return {
                ...preState,
                tags: {
                    ...preState.tags,
                    [id]: !this.state.tags[id]
                }
            }
        })
    }
    onChangeUpload = (info) => {
        if (info.file.status === 'uploading') {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === 'done') {
            if (info.file.response.code === 200) {
                this.setState({ coverUrl: info.file.response.data.url, loading: false })
            }
            else {
                message.error('上传失败');
                this.setState({ loading: false });
            }
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name}上传失败`);
        }
    }
    beforeUpload = (file) => {
        return new Promise((res, rej) => {
            if (file.size > 1024 * 1024 * 10) {
                message.info('上传图片大小不能超过10M')
                rej(file)
                return
            }
            res(file)
        })
    }
    onClickUserimg = (imgUrl, imgId) => {
        this.setState({ coverUrl: imgUrl, imgId })
    }
    onClickPublish = () => {
        const {location, history } = this.props;
        const { coverUrl, title, introduce, explain, tags, openFlag } = this.state;
        let tagIds = []
        Object.keys(tags).forEach(item => {
            tags[item] && tagIds.push(item)
        })
        let params = new URLSearchParams(location.search);
        let loginToken =  localStorage.getItem(LOGINTOKEN);
        let data = {
            loginToken: loginToken,
            productionId: params.get('productionId'),
            sourceType: '2',
            name: title,
            content: params.get('content'),
            coverUrl,
            intro: introduce,
            operateExplain: explain,
            tagIds: tagIds.join(','),
            openFlag,
            category: '1'
        }
        postPublishInfoFetch(data).then(function (response) {
            if (response.data.code !== 200) {
                message.error(response.data.error, 1)
                return
            }
            message.success('发布成功', 1)
            history.push(`/works-detail/${params.get('productionId')}`)
        })
            .catch(function (error) {
                console.log(error);
            });


    }
    onClickBack = () => {
        let params = new URLSearchParams(this.props.location.search);
        let id = params.get('productionId');
        window.location.href = `${jumpApi}?type=3&productionId=${id}`
    }

}
