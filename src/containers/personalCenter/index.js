
/**
 * 个人中心
 * */
import React from 'react'
import './index.less'
import Header from "../../components/header";
import Content from "../../components/content";
import GeneralEmpty from "../../components/generalEmpty";
import {
    Route,
    Link,
    Redirect,
    Switch
} from "react-router-dom";
import MyWorks from "../myWorks";
import AccountSettings from "../accountSettings";
import { connect } from 'react-redux'
import App from "../../App";
import {saveLoginInfo} from "../../redux/actions/loginactions";

@connect(({ login }) => ({ login }))
class PersonalCenter  extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {

        };


        console.log('PersonalCenter constructor ',this.props.location.pathname);

    }
    componentWillMount() {
        console.log('PersonalCenter componentWillMount ');
    }
    componentDidMount(){
        console.log('PersonalCenter componentDidMount ');
        // setTimeout(()=>{
        //     this.props.dispatch(saveLoginInfo(localStorage.getItem(LOGINTOKEN), window.localStorage.getItem('userInfo')))
        // },5000);
    }
    componentWillReceiveProps(nextProps){
        console.log('PersonalCenter componentWillReceiveProps',nextProps);
    }
    componentDidUpdate(prevProps, prevState, snapshot){
        console.log('PersonalCenter componentDidUpdate \n preProps', prevProps,'\n prevState', prevState,'\n snapshot',snapshot);
    }
    render(){

        console.log('PersonalCenter render',this.props.login);
        const {avatarUrl,name}  = localStorage.getItem('userInfo')&&JSON.parse(localStorage.getItem('userInfo'));

        let listArray = [{
            icon:require('../../static/img/icon-myworks-dark.png'),
            selectIcon:require('../../static/img/icon-myworks-light.png'),
            title:'我的作品',
            path:'/personal-center/works',
        },{
            icon:require('../../static/img/icon-mycounts-dark.png'),
            selectIcon:require('../../static/img/icon-mycounts-light.png'),
            title:'账号设置',
            path:'/personal-center/setting',
        }];
        let pathname = this.props.location.pathname;
        console.log('pathname',pathname);
        return <div>
            <Header activeType={2}/>
            <Content>
                <div className='personal-center-root'>
                    <div className='personal-center-content'>
                        <div className='left'>
                            <img className='head-portrait' src={avatarUrl}/>
                            <div className='name'>{name}</div>
                            <div className='divider'></div>
                            {listArray.map((item,index)=>{
                                let worksClass = (item.path === pathname)?'works select':'works';
                                let iconSrc = (item.path === pathname)?item.selectIcon:item.icon;
                                return <div key ={index} className={worksClass} onClick={()=>{
                                    this.props.history.push(item.path);
                                }}>
                                    <img className='icon'  src={iconSrc}/>{item.title}
                                </div>
                            })}

                        </div>
                        <div className='right'>
                            <Route exact path="/personal-center"  render={(props)=>
                                <Redirect to="/personal-center/works"/>
                            }/>
                            <Route path="/personal-center/works" component={MyWorks} />
                            <Route path="/personal-center/setting" component={AccountSettings} />
                            {/*<Route exact path="/personal-center" component={MyWorks} />*/}
                            {/*<Route path="/personal-center/setting" component={AccountSettings} />*/}
                        </div>
                    </div>
                </div>
            </Content>
        </div>
    }

}

export default PersonalCenter;
