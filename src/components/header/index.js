/**
 * 通用header
 * */
import React from 'react'
import './index.less'

import {saveLoginInfo} from "../../redux/actions/loginactions";
import {connect} from "react-redux";
import {jumpApi, LOGINTOKEN, USERINFO} from "../../utils/constants";

import {
    withRouter
} from "react-router-dom";
import { Icon,Input} from 'antd';


@connect(({ login }) => ({ login }))
class Header extends React.Component {

    // 构造
    constructor(props) {
        super(props);
        // 初始状态
        this.state = {};

        console.log('Header constructor', this.props);

    }

    componentWillMount() {
        console.log('Header componentWillMount ');
    }

    componentDidMount() {
        console.log('Header componentDidMount ');
    }

    componentWillReceiveProps(nextProps) {
        console.log('Header componentWillReceiveProps', nextProps);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('Header \n preProps', prevProps, '\n prevState', prevState, '\n snapshot', snapshot);
    }

    render() {
        console.log('Header render',this.props.login.userinfo);
        let userinfo = this.props.login.userinfo;
        let  headPortrait =  userinfo?userinfo.avatarUrl:require('../../static/img/header-img.png');
        console.log('headPortrait,',userinfo?userinfo.avatarUrl:'-------');
        let activeType = this.props.activeType?0:0; //目前忽略  课程管理 1  头像 2  退出 3
        return <div className='header-root'>
            <div className='header'>
                <div className='left'>
                    <div className='icon-logo' onClick={this.iconClick.bind(this)}>
                        <img className='icon-img'  src={require('../../static/img/logo.png')} alt=''/>
                    </div>
                    {this.courseContent.bind(this)()}
                </div>
                <div className='right'>
                    {this.props.hiddenCreation?'':
                        <div className='creation' onClick={this.creationClick.bind(this)}>
                            我要创作
                        </div>
                    }
                    <div className={`common-center-bg ${activeType===2?'active-hover':''}`} onClick={this.headPortraitClick.bind(this)}>
                        <img className='head-portrait' src={headPortrait} />
                    </div>
                    <div className={`common-center-bg ${activeType===3?'active-hover':''}`} onClick={this.logoutClick.bind(this)}
                         style={{
                        marginRight:'-15px',
                    }}>
                        <div className='logout'>
                            <Icon type="poweroff" className={'logout-icon'} /><span className={'logout-title'}>退出</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }

    courseContent = () => {
        console.log('this.props.type ',this.props.type);
        if (!this.props.type) {
            let activeType = this.props.activeType?0:0; //目前忽略  课程管理 1  头像 2  退出 3
            return  <div className={`course ${activeType===1?'active-hover':''}`} onClick={this.courseClick.bind(this)}>
                课程管理
            </div>
        }
        else if (this.props.type === 1) {
            return <div className='course-context-bg1'>
                <span>{this.props.title1}</span>
                <span>{this.props.title2}</span>
            </div>
        }
        else if (this.props.type === 2) {
            return <div className='course-context-bg2'>
                <div className='course-context-first' onClick={this.courseContextClick.bind(this)}>
                    <span>{this.props.title1}</span>
                    <span>{this.props.title2}</span>
                </div>
                <div className='arrow'>></div>
                <div className='course-context-second'>
                    <span>{this.props.title3}</span>
                    {this.props.title4&&this.props.title4.length?<span>{this.props.title4}</span>:''}
                </div>
            </div>
        }
        else if (this.props.type === 3) {
            return <div className='course-context-bg3'>
                <div className='course-context-first' onClick={this.courseContextClick.bind(this)}>
                    <span>{this.props.title1}</span>
                    <span>{this.props.title2}</span>
                </div>
                <div className='arrow'>></div>
                <div className='course-context-second' onClick={this.courseClassClick.bind(this)}>
                    <span>{this.props.title3}</span>
                    {this.props.title4&&this.props.title4.length?<span>{this.props.title4}</span>:''}
                </div>
                <div className='arrow'>></div>
                <div className='course-context-third'>
                    <span>{this.props.title5}</span>
                    {this.props.title6&&this.props.title6.length?<span>{this.props.title6}</span>:''}
                </div>
            </div>
        }
        else if (this.props.type === 4) {
            return <div className='course-home-bg'>
                 <div className='course-home' onClick={this.courseClick.bind(this)}>
                     课程管理
                 </div>
                <div className='search-bg'>
                    <Input className='search-input'  value={this.state.searchValue} placeholder="输入课程期次" onChange={this.onChange.bind(this)}/>
                    <div className='search-img-bg' onClick={this.searchKeywordClick.bind(this)}>
                        <div className='search-line'></div>
                        <img className='search-img' src={require('../../static/img/icon-search.png')}/>
                    </div>
                </div>
             </div>
        }
    }
    onChange = (e)=>{
        this.setState({
            searchValue:e.target.value,
        });
    }
    searchKeywordClick = ()=>{
        this.props.searchKeywordClick(this.state.searchValue);
    }
    iconClick = () => {
        if (this.state.searchValue)
        {
            this.setState({
                searchValue:'',
            });
            this.props.searchKeywordClick('');
            return;
        }
        this.props.history.push('/');
    }
    courseClick = () => {
        if (this.state.searchValue)
        {
            this.setState({
                searchValue:'',
            });
            this.props.searchKeywordClick('');
            return;
        }
        this.props.history.push('/');
    }
    courseContextClick = ()=>{
        this.props.history.goBack();
    }
    courseClassClick = ()=>{
        this.props.history.goBack();
    }
    creationClick = () => {
        window.open(`${jumpApi}?type=3&productionId=${0}`);
    }
    headPortraitClick = () => {
        this.props.history.push({
            pathname: '/personal-center/works',
            search: '?type=0'
        })
    }
    logoutClick = () => {
        localStorage.setItem(LOGINTOKEN, ''); //红涛
        localStorage.setItem(USERINFO, ''); //红涛
        let loginToken = localStorage.getItem(LOGINTOKEN);
        let userInfo = localStorage.getItem(USERINFO);;
        this.props.dispatch(saveLoginInfo(loginToken, userInfo));
        setTimeout(()=> {
            // this.props.history.replace('/login');
        },100)
    }


}

export default withRouter(Header);
